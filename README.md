# Museum Database Manager

Graphical manager of museum exhibits data written for Database classes. App allows to manage 
base data from some museum that holds exhibits made by artists with some properties.

Database model allows to manage also some events for exhibits like deports, hires and storages

## Database model

Database schema of the project model

![Database model](./resources/schema_en.svg)

Database can be creted with single [SQL script](./resources/tables_script_en.sql) written for PostgreSQL database.

## Used frameworks

Project written in Kotlin for desktop users with graphical interface. The database has to be created manually with 
specified script.

Used technologies:
* Gradle - management of compilation process
* TornadoFX - JavaFX framework to create graphical interface in Kotlin
* Exposed - JetBrains framework for database model management in Kotlin
* Spring Security Crypto - hashing users passwords for database storing

## Built result

[![built result](https://img.shields.io/gitlab/pipeline/avan1235/museum-database-manager/master)](https://gitlab.com/avan1235/museum-database-manager/-/jobs/artifacts/master/raw/build/jfx/app/museum-database-manager.jar?job=build)

Compiled version of program can be downloaded from [here](https://gitlab.com/avan1235/museum-database-manager/-/jobs/artifacts/master/raw/build/jfx/app/museum-database-manager.jar?job=build). As the database is not created by
this program also manual instalation of SQL file is needed in PostgreSQL database.

### Screenshots

![login screen](./resources/login.png)

![exhibits screen](./resources/exhibits.png)

![edit exhibits screen](./resources/edit_exhibit.png)

![artists screen](./resources/artists.png)

![edit artists screen](./resources/edit_artist.png)

![all data screen](./resources/all_data.png)

![others screen](./resources/others.png)