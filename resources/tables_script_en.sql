-- tables
-- Table: artist
CREATE TABLE artist (
    id serial  NOT NULL,
    name varchar(255)  NOT NULL,
    surname varchar(255)  NOT NULL,
    birth_year int  NOT NULL,
    death_year int  NULL,
    CONSTRAINT year CHECK (birth_year < death_year) NOT DEFERRABLE INITIALLY IMMEDIATE,
    CONSTRAINT artist_pk PRIMARY KEY (id)
);

-- Table: exhibit
CREATE TABLE exhibit (
    id serial  NOT NULL,
    title varchar(255)  NOT NULL,
    type varchar(127)  NOT NULL,
    height_cm int  NOT NULL,
    width_cm int  NOT NULL,
    weight_g int  NOT NULL,
    artist_id int  NULL,
    value_pln int  NOT NULL,
    CONSTRAINT exhibit_pk PRIMARY KEY (id)
);

-- Table: gallery
CREATE TABLE gallery (
    id serial  NOT NULL,
    name varchar(255)  NOT NULL,
    CONSTRAINT gallery_pk PRIMARY KEY (id)
);

-- Table: institution
CREATE TABLE institution (
    id serial  NOT NULL,
    name varchar(255)  NOT NULL,
    city varchar(255)  NOT NULL,
    CONSTRAINT institution_pk PRIMARY KEY (id)
);

-- Table: depot
CREATE TABLE depot (
    id serial  NOT NULL,
    name varchar(255)  NOT NULL,
    CONSTRAINT depot_pk PRIMARY KEY (id)
);

-- Table: storage
CREATE TABLE storage (
    id serial  NOT NULL,
    depot_id int  NOT NULL,
    store_case varchar(511)  NOT NULL,
    CONSTRAINT storage_pk PRIMARY KEY (id)
);

-- Table: room
CREATE TABLE room (
    id serial  NOT NULL,
    area_m2 real  NOT NULL,
    gallery_id int  NOT NULL,
    CONSTRAINT room_pk PRIMARY KEY (id)
);

-- Table: museum_db_user
CREATE TABLE museum_db_user (
    login varchar(31)  NOT NULL,
    password_hash varchar(255)  NOT NULL,
    mod boolean  NOT NULL,
    CONSTRAINT museum_db_user_pk PRIMARY KEY (login)
);

-- Table: museum_db_user_audit
CREATE TABLE museum_db_user_audit (
    id serial  NOT NULL,
    operacja varchar(1)  NOT NULL,
    czas timestamp  NOT NULL,
    museum_db_user_id text  NOT NULL,
    login varchar(31)  NOT NULL,
    password_hash varchar(255)  NOT NULL,
    mod boolean  NOT NULL,
    CONSTRAINT museum_db_user_audit_pk PRIMARY KEY (id)
);

CREATE OR REPLACE FUNCTION audit_museum_db_user() RETURNS TRIGGER AS $museum_db_user_audit$
    BEGIN
        IF (TG_OP = 'DELETE') THEN
            INSERT INTO museum_db_user_audit VALUES(DEFAULT, 'D', now(), user, OLD.*);
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            INSERT INTO museum_db_user_audit VALUES(DEFAULT, 'U', now(), user, NEW.*);
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            INSERT INTO museum_db_user_audit VALUES(DEFAULT, 'I', now(), user, NEW.*);
            RETURN NEW;
        END IF;
        RETURN NULL;
    END;
$museum_db_user_audit$ LANGUAGE plpgsql;

CREATE TRIGGER museum_db_user_audit
AFTER INSERT OR UPDATE OR DELETE ON museum_db_user
    FOR EACH ROW EXECUTE PROCEDURE audit_museum_db_user();

-- Table: hire
CREATE TABLE hire (
    id serial  NOT NULL,
    institution_id int  NOT NULL,
    CONSTRAINT hire_pk PRIMARY KEY (id)
);

-- Table: exhibition
CREATE TABLE exhibition (
    id serial  NOT NULL,
    room_id int  NOT NULL,
    CONSTRAINT exhibition_pk PRIMARY KEY (id)
);

-- Table: event_of_exhibit
CREATE TABLE event_of_exhibit (
    id serial  NOT NULL,
    exhibit_id int  NOT NULL,
    storage_id int  NULL,
    hire_id int  NULL,
    exhibition_id int  NULL,
    date_from date  NOT NULL,
    date_to date  NOT NULL,
    CONSTRAINT single_event CHECK ((CASE WHEN exhibition_id IS NULL THEN 0 ELSE 1 END + CASE WHEN hire_id IS NULL THEN 0 ELSE 1 END + CASE WHEN 
storage_id IS NULL THEN 0 ELSE 1 END) = 1) NOT DEFERRABLE INITIALLY IMMEDIATE,
    CONSTRAINT data CHECK (date_from <= date_to) NOT DEFERRABLE INITIALLY IMMEDIATE,
    CONSTRAINT event_of_exhibit_pk PRIMARY KEY (id)
);

-- special function for counting the days away from museum for exhibit in specified year
CREATE OR REPLACE FUNCTION hire_in_year(id int, year int)
RETURNS int AS $$
DECLARE days_sum INT;
BEGIN
        SELECT  GREATEST((SUM(LEAST(DATE($2::text || '-12-31'), date_to)::date - GREATEST(DATE($2::text || '-01-01'), date_from)::date)) + 1, 0) INTO days_sum
        FROM    event_of_exhibit
        WHERE   (hire_id IS NOT NULL) AND (exhibit_id = $1) AND ((DATE($2::text || '-01-01'), DATE($2::text || '-12-31')) OVERLAPS (date_from, 
date_to));

        RETURN days_sum;
END;
$$  LANGUAGE PLPGSQL;

-- foreign keys
-- Reference: exhibit_artist (table: exhibit)
ALTER TABLE exhibit ADD CONSTRAINT exhibit_artist
    FOREIGN KEY (artist_id)
    REFERENCES artist (id)
    ON DELETE  CASCADE  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: storage_depot (table: storage)
ALTER TABLE storage ADD CONSTRAINT storage_depot
    FOREIGN KEY (depot_id)
    REFERENCES depot (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: room_gallery (table: room)
ALTER TABLE room ADD CONSTRAINT room_gallery
    FOREIGN KEY (gallery_id)
    REFERENCES gallery (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: hire_institution (table: hire)
ALTER TABLE hire ADD CONSTRAINT hire_institution
    FOREIGN KEY (institution_id)
    REFERENCES institution (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: exhibition_room (table: exhibition)
ALTER TABLE exhibition ADD CONSTRAINT exhibition_room
    FOREIGN KEY (room_id)
    REFERENCES room (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: event_of_exhibit_exhibit (table: event_of_exhibit)
ALTER TABLE event_of_exhibit ADD CONSTRAINT event_of_exhibit_exhibit
    FOREIGN KEY (exhibit_id)
    REFERENCES exhibit (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: event_of_exhibit_storage (table: event_of_exhibit)
ALTER TABLE event_of_exhibit ADD CONSTRAINT event_of_exhibit_storage
    FOREIGN KEY (storage_id)
    REFERENCES storage (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: event_of_exhibit_hire (table: event_of_exhibit)
ALTER TABLE event_of_exhibit ADD CONSTRAINT event_of_exhibit_hire
    FOREIGN KEY (hire_id)
    REFERENCES hire (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: event_of_exhibit_exhibition (table: event_of_exhibit)
ALTER TABLE event_of_exhibit ADD CONSTRAINT event_of_exhibit_exhibition
    FOREIGN KEY (exhibition_id)
    REFERENCES exhibition (id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- End of file.

