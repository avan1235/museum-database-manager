package com.procyk.maciej.museum.controller

import com.procyk.maciej.museum.controller.EventController.Companion.HIRE_DAYS_LIMIT
import com.procyk.maciej.museum.data.*
import com.procyk.maciej.museum.database.entity.*
import com.procyk.maciej.museum.database.sql.hireInYear
import com.procyk.maciej.museum.database.table.EventTable
import com.procyk.maciej.museum.database.table.ExhibitionTable
import com.procyk.maciej.museum.database.table.HireTable
import com.procyk.maciej.museum.database.table.StorageTable
import com.procyk.maciej.museum.view.confirmDialog
import com.procyk.maciej.museum.view.screen.AppScreen
import com.procyk.maciej.museum.view.screen.EventWarningScreen
import com.procyk.maciej.museum.view.screen.ExhibitHistoryScreen
import com.procyk.maciej.museum.view.screen.HistoryEventEditScreen
import javafx.collections.ObservableList
import javafx.scene.control.Alert
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import tornadofx.*
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.time.LocalDate
import java.time.temporal.ChronoUnit

enum class HistoryEventType {
    HireEvent,
    StoreEvent,
    ExhibitionEvent
}

class EventController : Controller() {

    private val appScreen: AppScreen by inject()
    private val eventWarningScreen: EventWarningScreen by inject()
    private val exhibitHistoryScreen: ExhibitHistoryScreen by inject()
    private val historyEventEditScreen: HistoryEventEditScreen by inject()

    fun refreshEventData(data: ObservableList<EventData>) {
        data.asyncItems {
            transaction {
                EventEntity.all().map(EventEntity::toEventData).toList().sorted()
            }
        }
    }

    fun refreshEventDataForLoaded(data: ObservableList<EventData>, exhibitData: ExhibitData?) {
        exhibitData?.let { it.id?.let { id ->
            data.asyncItems {
                transaction {
                    EventEntity.find { EventTable.exhibitId eq id }.map(EventEntity::toEventData).toList().sorted()
                }
            }
        } }
    }

    fun openHistoryEventLogWindow(exhibitData: ExhibitData?, editMode: Boolean = false) {
        exhibitData?.let {
            exhibitHistoryScreen.reloadAndOpenWindow(it.copy(), editMode)
        }
    }

    fun openEditWindow(eventData: EventData?) {
        eventData?.let {
            historyEventEditScreen.reloadAndOpenWindow(it.copy())
        }
    }

    fun openCreateWindow(exhibitData: ExhibitData?) {
        exhibitData?.let {
            openEditWindow(EventData.getEmpty().copy(exhibit = it))
        }
    }

    fun deleteHistoryEvent(eventData: EventData?) {
        eventData?.let { event ->
            confirmDialog {
                val describedData = event.describedEventData
                transaction {
                    EventEntity.find { EventTable.id eq event.id }.forEach { it.delete() }
                    when(describedData) {
                        is StoreEventData -> transaction {
                            StorageEntity.find { StorageTable.id eq describedData.id }.forEach { it.delete() }
                        }
                        is HireEventData -> transaction {
                            HireEntity.find { HireTable.id eq describedData.id }.forEach { it.delete() }
                        }
                        is ExhibitionEventData -> transaction {
                            ExhibitionEntity.find { ExhibitionTable.id eq describedData.id }.forEach { it.delete() }
                        }
                    }
                }
            }
        }
    }

    fun getPossibleRooms(): List<RoomData> = transaction {
        RoomEntity.all().map(RoomEntity::toRoomData)
    }

    fun getPossibleInstitutions(): List<InstitutionData> = transaction {
        InstitutionEntity.all().map(InstitutionEntity::toInstitutionData)
    }

    fun getPossibleDepots(): List<DepotData> = transaction {
        DepotEntity.all().map(DepotEntity::toDepotData)
    }

    fun closeHistoryEventEditScreen() = historyEventEditScreen.close()

    fun closeEventWarningScreen() = eventWarningScreen.close()

    private fun findCollisionsInEvents(editedEventData: EventData, describedEventData: DescribedEventData, onExit: () -> Unit = { historyEventEditScreen.close() }) {
        transaction {
            val fullyDeleted = EventEntity.find { (EventTable.exhibitId eq editedEventData.exhibit.id) and
                    (EventTable.dateFrom greaterEq editedEventData.dateFrom) and
                    (EventTable.dateTo lessEq editedEventData.dateTo) }

            val partiallyChangedLeft = EventEntity.find { (EventTable.exhibitId eq editedEventData.exhibit.id) and
                    ((EventTable.dateFrom less editedEventData.dateFrom) and
                            (EventTable.dateTo greaterEq editedEventData.dateFrom) and
                            (EventTable.dateTo less editedEventData.dateTo)) }

            val partiallyChangedRight = EventEntity.find { (EventTable.exhibitId eq editedEventData.exhibit.id) and
                    ((EventTable.dateTo greater editedEventData.dateTo) and
                            (EventTable.dateFrom lessEq editedEventData.dateTo) and
                            (EventTable.dateFrom greater editedEventData.dateFrom)) }

            val theSame = EventEntity.find { (EventTable.exhibitId eq editedEventData.exhibit.id) and
                    (EventTable.dateFrom eq editedEventData.dateFrom) and
                    (EventTable.dateTo eq editedEventData.dateTo) }

            val separated = EventEntity.find { (EventTable.exhibitId eq editedEventData.exhibit.id) and
                    (EventTable.id neq editedEventData.id) and
                    (EventTable.dateFrom less editedEventData.dateFrom) and
                    (EventTable.dateTo greater editedEventData.dateTo) }

            val fullyDeletedData = fullyDeleted.map(EventEntity::toEventData)
            val partiallyChangedData = Pair(partiallyChangedLeft.map(EventEntity::toEventData), partiallyChangedRight.map(EventEntity::toEventData))
            val theSameData = theSame.map(EventEntity::toEventData)
            val separatedData = separated.map(EventEntity::toEventData)

            if (listOf(fullyDeletedData, partiallyChangedData.first, partiallyChangedData.second, theSameData, separatedData).map { it.size }.sum() == 0) {
                // proceed with no prompt
                proceedWithChangesFor(editedEventData, describedEventData, fullyDeletedData, partiallyChangedData, theSameData, separatedData)
                return@transaction
            }

            eventWarningScreen.reloadAndOpenWindow(fullyDeletedData, partiallyChangedData, theSameData, separatedData, editedEventData, describedEventData, onExit)
        }
    }

    fun saveEdited(editedEventData: EventData, storeEventData: StoreEventData, hireEventData: HireEventData, exhibitionEventData: ExhibitionEventData, eventTypeSelected: HistoryEventType) {
        confirmDialog {
            val describedEventData: DescribedEventData = when(eventTypeSelected) {
                HistoryEventType.StoreEvent -> storeEventData.copyDetailed()
                HistoryEventType.HireEvent -> hireEventData.copyDetailed()
                HistoryEventType.ExhibitionEvent -> exhibitionEventData.copyDetailed()
            }

            findCollisionsInEvents(editedEventData.copy(), describedEventData)
            historyEventEditScreen.close()
        }
    }

    fun proceedWithChangesFor(editedEventData: EventData, describedEventData: DescribedEventData, fullyDeletedData: List<EventData>, partiallyChangedData: Pair<List<EventData>, List<EventData>>, theSameData: List<EventData>, separatedData: List<EventData>) {
        transaction {
            val canBeChanged: Boolean = run {
                if (describedEventData !is HireEventData) {
                    return@run true
                }

                val hireFully = fullyDeletedData.filterIsType<HireEventData>()
                val hirePartially = Pair(partiallyChangedData.first.filterIsType<HireEventData>(), partiallyChangedData.second.filterIsType<HireEventData>())
                val hireSame = theSameData.filterIsType<HireEventData>()
                val hireSeparated = separatedData.filterIsType<HireEventData>()

                // check for 30 days constraint
                if (editedEventData.dateFrom.year == editedEventData.dateTo.year) {

                    if (editedEventData.length > HIRE_DAYS_LIMIT) {
                        // too long hire event so cannot be added
                        return@run false
                    }

                    // check after changes there are too many days of hire in specified year
                    return@run passHireDaysCheck(editedEventData.dateTo.year, editedEventData, hireFully, hirePartially, hireSame, hireSeparated)
                }
                else if (editedEventData.dateFrom.year + 1 == editedEventData.dateTo.year) {

                    val yFrom = editedEventData.dateFrom.year
                    val yTo = editedEventData.dateTo.year

                    val endYearDate = LocalDate.of(yFrom, 12, 31)
                    val beginYearDate = LocalDate.of(yTo, 1, 1)

                    return@run passHireDaysCheck(yFrom, editedEventData.copy(dateTo = endYearDate),
                            hireFully.filter { it.dateFrom.year == yFrom }.map { it.copy(dateTo = minOf(it.dateTo, endYearDate)) },
                            Pair(hirePartially.first.map { it.copy(dateTo = minOf(it.dateTo, endYearDate)) }, hirePartially.second.filter { it.dateFrom.year == yFrom }.map { it.copy(dateTo = endYearDate) }),
                            hireSame.map { it.copy(dateTo = endYearDate) },
                            hireSeparated)
                                &&
                                    passHireDaysCheck(yTo, editedEventData.copy(dateFrom = beginYearDate),
                                    hireFully.filter { it.dateTo.year == yTo }.map { it.copy(dateFrom = maxOf(it.dateFrom, beginYearDate)) },
                                    Pair(hirePartially.first.filter { it.dateTo.year == yTo }.map { it.copy(dateFrom = beginYearDate) }, hirePartially.second.map { it.copy(dateFrom = maxOf(it.dateFrom, beginYearDate)) }),
                                    hireSame.map { it.copy(dateFrom = beginYearDate) },
                                    hireSeparated)
                }
                return@run false
            }

            if (!canBeChanged) {
                alert(Alert.AlertType.WARNING, messages["Cannot proceed with specified because of 30 days limit per year on hire"])
                eventWarningScreen.closeModal()
                return@transaction
            }

            fullyDeletedData.forEach { EventEntity.findById(it.id!!)?.cascadeDelete() }
            partiallyChangedData.first.forEach { EventEntity.findById(it.id!!)?.dateTo = editedEventData.dateFrom.minusDays(1) }
            partiallyChangedData.second.forEach { EventEntity.findById(it.id!!)?.dateFrom = editedEventData.dateTo.plusDays(1) }
            theSameData.forEach { EventEntity.findById(it.id!!)?.cascadeDelete() }
            separatedData.forEach {
                EventEntity.findById(it.id!!)?.let { entity ->
                    val exhibitionData = it.describedEventData as? ExhibitionEventData
                    val hireData = it.describedEventData as? HireEventData
                    val storeData = it.describedEventData as? StoreEventData
                    when {
                        exhibitionData != null -> {
                            val newExhibition = ExhibitionEntity.new {
                                roomId = RoomEntity.findById(exhibitionData.roomData.id!!)!!.id
                            }
                            EventEntity.new {
                                dateFrom = editedEventData.dateTo.plusDays(1)
                                dateTo = entity.dateTo
                                exhibitId = entity.exhibitId
                                storageId = null
                                hireId = null
                                exhibitionId = newExhibition.id
                            }
                            entity.dateTo = editedEventData.dateFrom.minusDays(1)
                        }
                        hireData != null -> {
                            val newHire = HireEntity.new {
                                institutionId = InstitutionEntity.findById(hireData.institutionData.id!!)!!.id
                            }
                            EventEntity.new {
                                dateFrom = editedEventData.dateTo.plusDays(1)
                                dateTo = entity.dateTo
                                exhibitId = entity.exhibitId
                                storageId = null
                                hireId = newHire.id
                                exhibitionId = null
                            }
                            entity.dateTo = editedEventData.dateFrom.minusDays(1)
                        }
                        storeData != null -> {
                            val newStore = StorageEntity.new {
                                depotId = DepotEntity.findById(storeData.depotData.id!!)!!.id
                                storeCase = storeData.storeCase
                            }
                            EventEntity.new {
                                dateFrom = editedEventData.dateTo.plusDays(1)
                                dateTo = entity.dateTo
                                exhibitId = entity.exhibitId
                                storageId = newStore.id
                                hireId = null
                                exhibitionId = null
                            }
                            entity.dateTo = editedEventData.dateFrom.minusDays(1)
                        }
                        else -> { throw IllegalStateException() }
                    }
                }
            }

            editedEventData.id?.let { id ->
                EventEntity.findById(id)?.let { entity ->
                    connect(editedEventData, describedEventData, entity, getEventDetails(describedEventData))
                }
            } ?: run {
                EventEntity.new {
                    exhibitId = ExhibitEntity.findById(editedEventData.exhibit.id!!)!!.id
                    connect(editedEventData, describedEventData, this, getEventDetails(describedEventData))
                }
            }
            eventWarningScreen.closeModal()
        }
    }

    private fun List<EventData>.sorted()
            = this.sortedByDescending(EventData::dateFrom).sortedBy { eventData -> eventData.exhibit.id }

    companion object {
        const val HIRE_DAYS_LIMIT = 30
    }
}

private fun getEventDetails(describedEventData: DescribedEventData): EntityID<Int> {

    when {
        describedEventData is HireEventData -> {
            val hireId = describedEventData.id?.let { HireEntity.findById(it) } ?: HireEntity.new {
                institutionId = InstitutionEntity.findById(describedEventData.institutionData.id!!)!!.id
            }
            return hireId.id
        }
        describedEventData is StoreEventData -> {
            val storeId = describedEventData.id?.let { StorageEntity.findById(it) } ?: StorageEntity.new {
                depotId = DepotEntity.findById(describedEventData.depotData.id!!)!!.id
                storeCase = describedEventData.storeCase
            }
            return storeId.id
        }
        describedEventData is ExhibitionEventData -> {
            val exhibitionId = describedEventData.id?.let { ExhibitionEntity.findById(it) } ?: ExhibitionEntity.new {
                roomId = RoomEntity.findById(describedEventData.roomData.id!!)!!.id
            }
            return exhibitionId.id
        }
        else -> { throw IllegalStateException() }
    }
}

private fun connect(editedEventData: EventData, describedEventData: DescribedEventData, entity: EventEntity, connected: EntityID<Int>) {
    entity.dateFrom = editedEventData.dateFrom
    entity.dateTo = editedEventData.dateTo
    when {
        describedEventData is HireEventData -> {
            entity.hireId = connected
            entity.storageId = null
            entity.exhibitionId = null
        }
        describedEventData is StoreEventData -> {
            entity.hireId = null
            entity.storageId = connected
            entity.exhibitionId = null
        }
        describedEventData is ExhibitionEventData -> {
            entity.hireId = null
            entity.storageId = null
            entity.exhibitionId = connected
        }
        else -> { throw IllegalStateException() }
    }
}

private fun passHireDaysCheck(year: Int, editedEventData: EventData, fullyDeletedData: List<EventData>, partiallyChangedData: Pair<List<EventData>, List<EventData>>, theSameData: List<EventData>, separatedData: List<EventData>): Boolean {
    val oldDaysInYear = hireInYear(editedEventData.exhibit.id!!, year)
    val shortenedDays =
            fullyDeletedData.map(EventData::length).sum() +
                    partiallyChangedData.first.map { it.dateTo - editedEventData.dateFrom }.sum() +
                    partiallyChangedData.second.map { editedEventData.dateTo - it.dateFrom }.sum() +
                    theSameData.map(EventData::length).sum() +
                    (separatedData.size * editedEventData.length)

    val afterDays = oldDaysInYear - shortenedDays + editedEventData.length

    if (afterDays > HIRE_DAYS_LIMIT) {
        // error in days for this year counting
        return false
    }

    return true
}

fun EventEntity.cascadeDelete() {
    val storageId = this.storageId
    val hireId = this.hireId
    val exhibitionId = this.exhibitionId
    this.delete()
    when {
        exhibitionId != null -> ExhibitionEntity.findById(exhibitionId)?.delete()
        storageId != null -> StorageEntity.findById(storageId)?.delete()
        hireId != null -> HireEntity.findById(hireId)?.delete()
    }
}

fun EventEntity.toEventData(): EventData {
    val eventEntity = this
    val exhibitionId = eventEntity.exhibitionId
    val hireId = eventEntity.hireId
    val storageId = eventEntity.storageId
    when {
        exhibitionId != null -> {
            return EventData(eventEntity.id.value, ExhibitionEntity.findById(exhibitionId)!!.toExhibitionEventData(),
                    ExhibitEntity.findById(eventEntity.exhibitId)!!.toExhibitData(),
                    eventEntity.dateFrom, eventEntity.dateTo)
        }
        hireId != null -> {
            return EventData(eventEntity.id.value, HireEntity.findById(hireId)!!.toHireEventData(),
                    ExhibitEntity.findById(eventEntity.exhibitId)!!.toExhibitData(),
                    eventEntity.dateFrom, eventEntity.dateTo)
        }
        storageId != null -> {
            return EventData(eventEntity.id.value, StorageEntity.findById(storageId)!!.toStoreEventData(),
                    ExhibitEntity.findById(eventEntity.exhibitId)!!.toExhibitData(),
                    eventEntity.dateFrom, eventEntity.dateTo)
        }
        else -> throw IllegalStateException("Illegal EventEntity found of id = $id")
    }
}

fun GalleryEntity.toGalleryData() = GalleryData(id.value, name)

fun RoomEntity.toRoomData() = RoomData(id.value, areaM, GalleryEntity.findById(galleryId)!!.toGalleryData())

fun InstitutionEntity.toInstitutionData() = InstitutionData(id.value, name, city)

fun DepotEntity.toDepotData() = DepotData(id.value, name)

fun ExhibitionEntity.toExhibitionEventData() = ExhibitionEventData(id.value, RoomEntity.findById(roomId)!!.toRoomData())

fun HireEntity.toHireEventData() = HireEventData(id.value, InstitutionEntity.findById(institutionId)!!.toInstitutionData())

fun StorageEntity.toStoreEventData() = StoreEventData(id.value, storeCase, DepotEntity.findById(depotId)!!.toDepotData())

operator fun LocalDate.minus(date: LocalDate): Long {
    if (!this.isAfter(date))
        throw IllegalArgumentException("Cannot subtract the date $date from $this")

    return date.until(this, ChronoUnit.DAYS) + 1
}

inline fun <reified T : DescribedEventData> Iterable<EventData>.filterIsType(): List<EventData> {
    return this.filter { it.describedEventData is T }
}

