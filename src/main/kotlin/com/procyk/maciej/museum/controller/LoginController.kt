package com.procyk.maciej.museum.controller

import com.procyk.maciej.museum.database.entity.UserEntity
import com.procyk.maciej.museum.tool.crypto.Crypto
import com.procyk.maciej.museum.view.screen.LoginScreen
import com.procyk.maciej.museum.view.screen.AppScreen
import org.jetbrains.exposed.sql.transactions.transaction
import tornadofx.*

class LoginController : Controller() {

    private val loginScreen: LoginScreen by inject()
    private val appScreen: AppScreen by inject()

    companion object {
        var loggedMod = false
    }

    private fun showLoginScreen(shake: Boolean = false) {
        appScreen.replaceWith(loginScreen, sizeToScene = true, centerOnScreen = true)
        if (shake) runLater { loginScreen.shake() }
    }

    private fun showApplicationScreen() {
        appScreen.reloadWindow()
        loginScreen.replaceWith(appScreen, sizeToScene = true, centerOnScreen = true)
    }

    fun tryLogin(username: String, password: String) {
        runAsync {
            transaction {
                UserEntity.findById(username)?.let {
                    return@transaction Crypto.isValidPassword(password, it.passwordHash)
                } ?: return@transaction false
            }
        } ui { successfulLogin ->
            if (successfulLogin) {
                markLoggedState(username)
                loginScreen.clear()
                showApplicationScreen()
            } else {
                showLoginScreen(true)
            }
        }
    }

    private fun markLoggedState(username: String) {
        loggedMod = transaction { UserEntity.findById(username)?.mod ?: false }
    }

    fun isLoggedMod() = loggedMod

    fun logout() {
        showLoginScreen()
    }
}