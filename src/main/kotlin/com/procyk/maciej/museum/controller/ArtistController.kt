package com.procyk.maciej.museum.controller

import com.procyk.maciej.museum.data.ArtistData
import com.procyk.maciej.museum.database.entity.ArtistEntity
import com.procyk.maciej.museum.database.entity.EventEntity
import com.procyk.maciej.museum.database.entity.ExhibitEntity
import com.procyk.maciej.museum.database.table.EventTable
import com.procyk.maciej.museum.database.table.ExhibitTable
import com.procyk.maciej.museum.view.confirmDialog
import com.procyk.maciej.museum.view.eqNullable
import com.procyk.maciej.museum.view.screen.AppScreen
import com.procyk.maciej.museum.view.screen.ArtistEditScreen
import javafx.collections.ObservableList
import org.jetbrains.exposed.sql.transactions.transaction
import tornadofx.*
import tornadofx.controlsfx.confirmNotification

class ArtistController : Controller() {

    private val appScreen: AppScreen by inject()
    private val artistEditScreen: ArtistEditScreen by inject()

    fun refreshArtistsData(data: ObservableList<ArtistData> = appScreen.tableArtistsData) {
        data.asyncItems {
            transaction {
                ArtistEntity.all().map(ArtistEntity::toArtistData).toList()
            }
        }
    }

    fun openEditWindow(artistData: ArtistData?) {
        artistData?.let {
            artistEditScreen.reloadAndOpenWindow(it.copy())
        }
    }

    fun saveEdited(artistData: ArtistData) {
        artistData.id?.let { id ->
            confirmDialog {
                transaction {
                    val artistEntity = ArtistEntity.findById(id)
                    artistEntity?.name = artistData.name
                    artistEntity?.surname = artistData.surname
                    artistEntity?.birthYear = artistData.birthYear
                    artistEntity?.deathYear = artistData.deathYear
                }
                refreshArtistsData()
                closeArtistEditScreen()
            }
        }
    }

    fun closeArtistEditScreen() = artistEditScreen.close()

    fun getPossibleArtists(): List<ArtistData> {
        val artists =  transaction { ArtistEntity.all().map { it.toArtistData() } }.toMutableList()
        artists.add(ArtistData.EMPTY)
        return artists
    }

    fun deleteArtistWithExhibitsAndHistory(artistData: ArtistData?) {
        artistData?.let { artist -> artist.id?.let { id ->
            confirmDialog {
                transaction {
                    ExhibitEntity.find { ExhibitTable.artistId eqNullable id }.forEach { exhibit ->
                        val exhibitId = exhibit.id
                        EventEntity.find { EventTable.exhibitId eq exhibitId }.forEach { event -> event.cascadeDelete() }
                        exhibit.delete()
                    }
                    ArtistEntity.findById(id)?.delete()
                }
            }
        } }
    }
}

fun ArtistEntity.toArtistData() = ArtistData(id.value, name, surname, birthYear, deathYear)