package com.procyk.maciej.museum.controller

import com.procyk.maciej.museum.data.ArtistData
import com.procyk.maciej.museum.data.EventData
import com.procyk.maciej.museum.data.ExhibitData
import com.procyk.maciej.museum.database.entity.*
import com.procyk.maciej.museum.database.table.EventTable
import com.procyk.maciej.museum.database.table.ExhibitTable
import com.procyk.maciej.museum.view.confirmDialog
import com.procyk.maciej.museum.view.eqNullable
import com.procyk.maciej.museum.view.screen.AppScreen
import com.procyk.maciej.museum.view.screen.ArtistExhibitsScreen
import com.procyk.maciej.museum.view.screen.ExhibitEditScreen
import javafx.collections.ObservableList
import org.controlsfx.dialog.CommandLinksDialog
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.transactions.transaction
import tornadofx.*

class ExhibitController : Controller() {

    private val appScreen: AppScreen by inject()
    private val exhibitEditScreen: ExhibitEditScreen by inject()
    private val artistExhibitsScreen: ArtistExhibitsScreen by inject()

    fun refreshExhibitData(data: ObservableList<ExhibitData> = appScreen.tableExhibitsData) {
        data.asyncItems {
            transaction {
                ExhibitEntity.all().map(ExhibitEntity::toExhibitData).toList()
            }
        }
    }

    fun openEditWindow(exhibitData: ExhibitData?) {
        exhibitData?.let {
            exhibitEditScreen.reloadAndOpenWindow(it.copy())
        }
    }

    fun openCreateWindow() {
        exhibitEditScreen.reloadAndOpenWindow(ExhibitData.getEmpty())
    }

    fun openArtistExhibitsWindow(artistData: ArtistData?, editMode: Boolean = false) {
        artistData?.let {
            artistExhibitsScreen.reloadAndOpenWindow(it.copy(), editMode)
        }
    }

    private fun getArtistForExhibit(artistData: ArtistData?, exhibitEntity: ExhibitEntity? = null): EntityID<Int>? {
        return when(artistData) {
            null -> null
            else -> {
                var artistID = artistData.id
                if (artistID == null || ArtistEntity.findById(artistID) == null) {
                    artistID = ArtistEntity.new {
                        name = artistData.name
                        surname = artistData.surname
                        birthYear = artistData.birthYear
                        deathYear = artistData.deathYear
                    }.id.value
                }
                ArtistEntity.findById(artistID)?.id ?: exhibitEntity?.artistId
            }
        }
    }

    fun refreshEventDataForLoaded(data: ObservableList<ExhibitData>, artistData: ArtistData?) {
        artistData?.let { it.id?.let { id ->
            data.asyncItems {
                transaction {
                    ExhibitEntity.find { ExhibitTable.artistId eqNullable id }.map(ExhibitEntity::toExhibitData).toList()
                }
            }
        } }
    }

    fun saveEdited(exhibitData: ExhibitData) {
        exhibitData.id?.let {  id ->
            confirmDialog {
                transaction {
                    ExhibitEntity.findById(id)?.let { entity ->
                        entity.title = exhibitData.title
                        entity.type = exhibitData.type
                        entity.heightCm = exhibitData.heightCm
                        entity.widthCm = exhibitData.widthCm
                        entity.valuePln = exhibitData.valuePln
                        entity.weightG = exhibitData.weightG
                        entity.artistId = getArtistForExhibit(exhibitData.artist, entity)
                    }
                }
                refreshExhibitData()
                closeExhibitEditScreen()
            }
        } ?: run {
            confirmDialog {
                transaction {
                    ExhibitEntity.new {
                        title = exhibitData.title
                        type = exhibitData.type
                        heightCm = exhibitData.heightCm
                        widthCm = exhibitData.widthCm
                        valuePln = exhibitData.valuePln
                        weightG = exhibitData.weightG
                        artistId = getArtistForExhibit(exhibitData.artist)
                    }
                }
                refreshExhibitData()
                closeExhibitEditScreen()
            }
        }
    }

    fun closeExhibitEditScreen() = exhibitEditScreen.close()

    fun deleteExhibitWithHistory(exhibitData: ExhibitData?) {
        exhibitData?.let {
            confirmDialog {
                transaction {
                    EventEntity.find { EventTable.exhibitId eq it.id }.forEach { it.cascadeDelete() }
                    ExhibitEntity.find { ExhibitTable.id eq it.id }.forEach { it.delete() }
                }
            }
        }
    }
}

fun ExhibitEntity.toExhibitData() = ExhibitData(id.value, title, type, heightCm, widthCm, weightG, artistId?.let { ArtistEntity.findById(it)?.toArtistData() }, valuePln)