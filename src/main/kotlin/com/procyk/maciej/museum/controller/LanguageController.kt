package com.procyk.maciej.museum.controller

import com.procyk.maciej.museum.app.Language
import tornadofx.*
import java.util.*

class LanguageController : Controller() {

    companion object {
        val LANGUAGE_SETTING = "lang"
    }

    fun loadLanguageFromConfig() {
        when(app.config.string(LANGUAGE_SETTING)) {
            "pl" -> FX.messages = ResourceBundle.getBundle("messages", Locale("pl"))
            "en" -> FX.messages = ResourceBundle.getBundle("messages", Locale("en"))
            else -> FX.messages = ResourceBundle.getBundle("messages", Language.DEFAULT_LANGUAGE.toLocale())
        }
    }

    fun saveToConfig(language: Language) {
        with(app.config) {
            when(language) {
                Language.Polish -> set(LANGUAGE_SETTING to "pl")
                Language.English -> set(LANGUAGE_SETTING to "en")
            }
            save()
        }
    }
}