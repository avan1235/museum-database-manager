package com.procyk.maciej.museum.controller

import com.procyk.maciej.museum.data.*
import com.procyk.maciej.museum.database.entity.*
import com.procyk.maciej.museum.database.table.EventTable
import com.procyk.maciej.museum.database.table.ExhibitTable
import com.procyk.maciej.museum.view.confirmDialog
import com.procyk.maciej.museum.view.screen.AppScreen
import org.jetbrains.exposed.sql.transactions.transaction
import tornadofx.*

class OthersController : Controller() {

    private val appScreen: AppScreen by inject()

    fun saveCreated(depotData: DepotData) {
        confirmDialog {
            transaction {
                DepotEntity.new {
                    name = depotData.name
                }
            }
        }
    }

    fun saveCreated(institutionData: InstitutionData) {
        confirmDialog {
            transaction {
                InstitutionEntity.new {
                    name = institutionData.name
                    city = institutionData.city
                }
            }
        }
    }

    fun saveCreated(galleryData: GalleryData) {
        confirmDialog {
            transaction {
                GalleryEntity.new {
                    name = galleryData.name
                }
            }
        }
    }

    fun saveCreated(roomData: RoomData) {
        roomData.galleryData.id?.let { galleryDataId ->
            confirmDialog {
                transaction {
                    RoomEntity.new {
                        areaM = roomData.areaM
                        galleryId = GalleryEntity.findById(galleryDataId)!!.id
                    }
                }
            }
        }
    }

    fun getPossibleGalleries(): List<GalleryData> {
        return transaction { GalleryEntity.all().map(GalleryEntity::toGalleryData) }
    }
}