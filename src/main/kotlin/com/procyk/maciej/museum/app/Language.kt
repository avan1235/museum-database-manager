package com.procyk.maciej.museum.app

import java.util.*

enum class Language(private val description: String, private val loc: Locale) {
    Polish("polski", Locale("pl")),
    English("english", Locale("en"));

    override fun toString(): String {
        return description
    }

    fun toLocale() = loc

    companion object {
        val DEFAULT_LANGUAGE = Polish

        fun fromLocale(locale: Locale) = when(locale.toLanguageTag()) {
            "pl" -> Polish
            "en" -> English
            else -> DEFAULT_LANGUAGE
        }
    }
}