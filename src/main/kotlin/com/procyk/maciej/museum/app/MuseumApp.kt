package com.procyk.maciej.museum.app

import com.procyk.maciej.museum.controller.LanguageController
import com.procyk.maciej.museum.database.settings.DbSettings
import com.procyk.maciej.museum.view.screen.LoginScreen
import javafx.stage.Stage
import tornadofx.*


class MuseumApp: App(LoginScreen::class, Styles::class) {

    companion object {
        private val db = DbSettings.connect
    }
}