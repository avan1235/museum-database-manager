package com.procyk.maciej.museum.app

import javafx.geometry.Pos
import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class Styles : Stylesheet() {
    companion object {
        val heading by cssclass()
        val loginScreen by cssclass()
        val centeredColumn by cssclass()
    }

    init {
        label and heading {
            padding = box(10.px)
            fontSize = 20.px
            fontWeight = FontWeight.BOLD
        }

        loginScreen {
            padding = box(10.px)
            vgap = 7.px
            hgap = 10.px
        }

        centeredColumn {
            alignment = Pos.BASELINE_CENTER
        }
    }
}