import com.procyk.maciej.museum.tool.crypto.Crypto
import java.lang.IllegalArgumentException
import java.time.LocalDate
import java.time.temporal.ChronoUnit

fun generateInsertQuery(user: String, password: String, mod: Boolean = true, usersTable: String = "museum_db_user") =
        "insert into $usersTable values ('$user', '${Crypto.hashPassword(password)}', ${if (mod) "true" else "false"});"

fun main() {
    println(generateInsertQuery(user = "scott", password = "tiger"))
    println(generateInsertQuery(user = "zbyszek", password = "admin", mod = false))
}