package com.procyk.maciej.museum.tool.crypto

import org.springframework.security.crypto.bcrypt.BCrypt

object Crypto {
    fun isValidPassword(password: String, passwordHash: String): Boolean =
            BCrypt.checkpw(password, passwordHash)


    fun hashPassword(password: String): String =
            BCrypt.hashpw(password, BCrypt.gensalt()) ?: password
}