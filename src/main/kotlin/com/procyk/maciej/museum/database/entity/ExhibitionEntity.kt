package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.ExhibitionTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class ExhibitionEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<ExhibitionEntity>(ExhibitionTable)

    var roomId by ExhibitionTable.roomId
}