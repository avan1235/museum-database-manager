package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.ArtistTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class ArtistEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<ArtistEntity>(ArtistTable)

    var name by ArtistTable.name
    var surname by ArtistTable.surname
    var birthYear by ArtistTable.birthYear
    var deathYear by ArtistTable.deathYear
}