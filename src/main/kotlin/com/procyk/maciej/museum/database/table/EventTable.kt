package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.date

object EventTable : IntIdTable("event_of_exhibit") {

    val exhibitId = reference("exhibit_id", ExhibitTable)
    val storageId = reference("storage_id", StorageTable).nullable()
    val hireId = reference("hire_id", HireTable).nullable()
    val exhibitionId = reference("exhibition_id", ExhibitionTable).nullable()
    val dateFrom = date("date_from")
    val dateTo = date("date_to")
}