package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object ArtistTable : IntIdTable("artist") {

    val name = varchar("name", 255)
    val surname = varchar("surname", 255)
    val birthYear = integer("birth_year")
    val deathYear = integer("death_year").nullable()
}