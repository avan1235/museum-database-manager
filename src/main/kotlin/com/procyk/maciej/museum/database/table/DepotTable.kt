package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object DepotTable : IntIdTable("depot") {

    val name = varchar("name", 255)
}