package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.RoomTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class RoomEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<RoomEntity>(RoomTable)

    var areaM by RoomTable.areaM
    var galleryId by RoomTable.galleryId
}