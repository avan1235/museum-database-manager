package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.UserTable
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.EntityID

class UserEntity(login: EntityID<String>) : Entity<String>(login) {

    companion object : EntityClass<String, UserEntity>(UserTable)

    val username by UserTable.id
    val passwordHash by UserTable.passwordHash
    val mod by UserTable.mod
}