package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object GalleryTable : IntIdTable("gallery") {

    val name = varchar("name", 255)
}