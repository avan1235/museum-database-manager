package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.InstitutionTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class InstitutionEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<InstitutionEntity>(InstitutionTable)

    var name by InstitutionTable.name
    var city by InstitutionTable.city
}