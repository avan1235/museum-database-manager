package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object HireTable : IntIdTable("hire") {

    val institutionId = reference("institution_id", InstitutionTable)
}