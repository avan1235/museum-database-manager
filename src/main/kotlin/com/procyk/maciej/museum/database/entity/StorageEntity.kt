package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.StorageTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class StorageEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<StorageEntity>(StorageTable)

    var depotId by StorageTable.depotId
    var storeCase by StorageTable.storeCase
}