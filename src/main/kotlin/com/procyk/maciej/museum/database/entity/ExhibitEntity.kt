package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.ExhibitTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class ExhibitEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<ExhibitEntity>(ExhibitTable)

    var title by ExhibitTable.title
    var type by ExhibitTable.type
    var heightCm by ExhibitTable.heightCm
    var widthCm by ExhibitTable.widthCm
    var weightG by ExhibitTable.weightG
    var artistId by ExhibitTable.artistId
    var valuePln by ExhibitTable.valuePln
}