package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object ExhibitionTable : IntIdTable("exhibition") {

    val roomId = reference("room_id", RoomTable)
}