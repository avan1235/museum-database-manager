package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object ExhibitTable : IntIdTable("exhibit") {

    val title = varchar("title", 255)
    val type = varchar("type", 127)
    val heightCm = integer("height_cm")
    val widthCm = integer("width_cm")
    val weightG = integer("weight_g")
    val artistId = reference("artist_id", ArtistTable).nullable()
    val valuePln = integer("value_pln")
}