package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object RoomTable : IntIdTable("room") {

    val areaM = double("area_m2")
    val galleryId = reference("gallery_id", GalleryTable)
}