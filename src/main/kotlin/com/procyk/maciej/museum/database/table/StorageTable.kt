package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object StorageTable : IntIdTable("storage") {

    val depotId = reference("depot_id", DepotTable)
    val storeCase = varchar("store_case", 511)
}