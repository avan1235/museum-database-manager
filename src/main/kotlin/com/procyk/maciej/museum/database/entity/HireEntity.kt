package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.HireTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class HireEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<HireEntity>(HireTable)

    var institutionId by HireTable.institutionId
}