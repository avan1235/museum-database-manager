package com.procyk.maciej.museum.database.settings

import org.jetbrains.exposed.sql.Database

object DbSettings {
    private const val DB_USERNAME = "mp406304"
    private const val DB_PASSWORD = "P4SSVV0RD"

    val connect by lazy {
        Database.connect(
                url = "jdbc:postgresql://labdb:5432/bd",
                driver = "org.postgresql.Driver",
                user = DB_USERNAME,
                password = DB_PASSWORD)
    }
}
