package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.DepotTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class DepotEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<DepotEntity>(DepotTable)

    var name by DepotTable.name
}