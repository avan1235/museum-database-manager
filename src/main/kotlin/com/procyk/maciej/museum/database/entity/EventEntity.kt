package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.EventTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class EventEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<EventEntity>(EventTable)

    var exhibitId by EventTable.exhibitId
    var storageId by EventTable.storageId
    var hireId by EventTable.hireId
    var exhibitionId by EventTable.exhibitionId
    var dateFrom by EventTable.dateFrom
    var dateTo by EventTable.dateTo

}