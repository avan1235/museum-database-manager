package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IntIdTable

object InstitutionTable : IntIdTable("institution") {

    val name = varchar("name", 255)
    val city = varchar("city", 255)
}