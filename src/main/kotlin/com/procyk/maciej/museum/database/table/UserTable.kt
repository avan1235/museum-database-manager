package com.procyk.maciej.museum.database.table

import org.jetbrains.exposed.dao.IdTable

object UserTable : IdTable<String>("museum_db_user") {

    override val id = varchar("login", 31).primaryKey().entityId()
    val passwordHash = varchar("password_hash", 255)
    val mod = bool("mod")
}