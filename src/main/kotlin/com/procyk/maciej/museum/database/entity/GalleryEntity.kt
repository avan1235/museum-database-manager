package com.procyk.maciej.museum.database.entity

import com.procyk.maciej.museum.database.table.GalleryTable
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class GalleryEntity(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<GalleryEntity>(GalleryTable)

    var name by GalleryTable.name
}