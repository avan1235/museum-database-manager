package com.procyk.maciej.museum.database.sql

fun hireInYear(exhibitId: Int, year: Int): Int {
    return "select hire_in_year($exhibitId, $year)".execAndMap {
        it.getInt("hire_in_year")
    }.first()
}