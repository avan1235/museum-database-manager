package com.procyk.maciej.museum.data

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*

class ArtistData(id: Int?, name: String, surname: String,
                 birthYear: Int, deathYear: Int? = null) : AbstractIdData() {
    companion object {
        fun getEmpty() = ArtistData(null, "", "", 1, null)
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty(id)
    override val id by idProperty

    val nameProperty = SimpleStringProperty(name)
    var name by nameProperty

    val surnameProperty = SimpleStringProperty(surname)
    var surname by surnameProperty

    val birthYearProperty = SimpleIntegerProperty(birthYear)
    var birthYear by birthYearProperty

    val deathYearProperty = SimpleObjectProperty(deathYear)
    var deathYear by deathYearProperty

    val isAlive: Boolean
        get() { return deathYear == null }

    override fun toString() = "$name $surname"

    fun copy(id: Int? = this.id, name: String = this.name, surname: String = this.surname,
             birthYear: Int = this.birthYear, deathYear: Int? = this.deathYear) = ArtistData(id, name, surname, birthYear, deathYear)
}