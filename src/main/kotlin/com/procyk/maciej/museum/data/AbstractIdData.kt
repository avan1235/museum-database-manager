package com.procyk.maciej.museum.data

abstract class AbstractIdData {
    abstract val id: Int?

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is AbstractIdData) return false
        if (id != other.id || javaClass != other.javaClass) return false

        return true
    }

    override fun hashCode(): Int {
        return arrayOf(id, javaClass).contentDeepHashCode()
    }
}