package com.procyk.maciej.museum.data

import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import tornadofx.FX.Companion.messages

class RoomData(id: Int?, areaM: Double, galleryData: GalleryData) : AbstractIdData() {

    companion object {
        fun getEmpty() = RoomData(null, 1.0, GalleryData.getEmpty())
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty(id)
    override val id by idProperty

    val areaMProperty = SimpleDoubleProperty(areaM)
    var areaM by areaMProperty

    val galleryDataProperty = SimpleObjectProperty(galleryData)
    var galleryData by galleryDataProperty

    override fun toString() = "${messages["Room"]}${if (id != null) " $id " else " "}${messages["from"]} $galleryData ${messages["of area"]} $areaM m\u00B2"

    fun copy(id: Int? = this.id, areaM: Double = this.areaM, galleryData: GalleryData = this.galleryData.copy()) = RoomData(id, areaM, galleryData)
}