package com.procyk.maciej.museum.data.generator

import com.procyk.maciej.museum.database.entity.*
import com.procyk.maciej.museum.database.settings.DbSettings
import io.github.serpro69.kfaker.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDate

val fakerConfig = FakerConfig.builder().create {
    locale = "pl"
}
val FAKER = Faker(fakerConfig)

fun generateArtists(times: Int = 60) {

    transaction {
        repeat(times) {
            val nameSet = FAKER.name.firstName()
            val surnameSet = FAKER.name.lastName()
            val birthSet = (1500..2000).random()
            val deathSet = birthSet + (30..100).random()
            ArtistEntity.new {
                name = nameSet
                surname = surnameSet
                birthYear = birthSet
                deathYear = if (deathSet >= LocalDate.now().year) null else deathSet
            }
        }
    }
}

fun generateExhibits() {

    val artistsIds = transaction { ArtistEntity.all().map { it.id.value } }.toSet()
    val types = listOf("obraz olejny", "rzeźba", "płaskorzeźba", "obraz")
    val titles = listOf("Portret damy w czarnej sukni", "Dziewczynka z chryzantemami", "Bitwa pod Chocimiem", "Bogurodzica", "Czarniecki pod Koldyngą", "Niebezpieczna przeprawa", "Odbicie jasyru", "Chrzest Litwy", "Dama przy oknie", "Dama w liliowej sukni z kwiatami (Dla niego)", "Martwa natura z czaplą", "Portret Jadwigi Sienkiewiczówny", "Kozak w stepie", "Widok na pomnik Jana III Sobieskiego na Agrykoli", "Jan Sabała Krzeptowski", "Piaskarze", "Plac Wittelsbachów w Monachium w nocy", "Trumna chłopska", "Żydówka z cytrynami", "Pijak", "Zamek w Wiśniczu", "Gęsiarka", "Trzy Marie u grobu Chrystusa", "Autoportret", "Paryż", "Rozbiórka kościoła świętej Klary i klasztoru Bernardynek", "Portret pani Tabęckiej", "Portret własny", "Gęsiarka", "Portret mężczyzny w krymce", "Góral", "Młyn w Prądniku", "Neapolitanka", "Pejzaż nadmorski", "Portret Józefa Piłsudskiego", "Śmierć Priama", "Dziewczynka z kanarkiem", "Sejm bociani", "Portret żony", "W letnim mieszkaniu", "Autoportret z paletą", "Amazonka", "Błękitny chłopiec", "Rajtar", "Szarża w wąwozie Somosierra", "Portret pośmiertny", "W żniwa", "Bitwa pod Zborowem", "Karykatura architekta", "Kirgizi", "Siedzący żołnierz", "Martwa natura z zielonym dzbanem", "Portret Adama Mickiewicza", "Portret własny z paletą", "Autoportret", "Martwa natura z czerwoną książką", "Na zesłanie w Sybir", "Dziewczyna z gołąbkiem", "Gęsiarka", "Most zimą", "Przedwiośnie", "Czaty kozackie", "Owocobranie", "Portret własny", "Autoportret", "Barbakan w Krakowie", "Napoleon i Franciszek II po bitwie pod Austerlitz", "Machabeusze", "Portret Tekli Bierkowskiej", "Odwrót spod Moskwy, epizod z roku 1812", "Portret konny Paskiewicza", "Portret Józefa Brandta", "Pogrzeb chłopski", "Widok na chór i nawę główną Katedry na Wawelu", "Autoportret", "Napoleon przy ognisku", "Stara Warszawa zimą -", "Trójka Syberyjska – Na Uralu", "Burłacy", "Brama Floriańska w Krakowie")
    val prices = (50..100).map { it * 1_000 }.shuffled()
    transaction {
        for (titleSet in titles) {
            val typeSet = types.random()
            val hSet = (50..300).random()
            val wSet = (40..200).random()
            val gSet = (1_000..100_000).random()
            val artSet = ArtistEntity.findById(artistsIds.random())
            val vSet = prices.random()
            val hasArtist = (1..100).random() > 10
            ExhibitEntity.new {
                title = titleSet
                type = typeSet
                heightCm = hSet
                widthCm = wSet
                weightG = gSet
                artistId = if (hasArtist) artSet!!.id else null
                valuePln = vSet
            }
        }
    }
    transaction {
        val ids = ExhibitEntity.all().map { it.artistId }.filterNotNull().map { it.value }.toSet()
        ArtistEntity.all().forEach { if (!ids.contains(it.id.value)) it.delete() }
    }
}

fun generateDepots() {

    val names = listOf("Magazyn Wschodni", "Magazyn Zachodni", "Magazyn Tymczasowy", "Magazyn Północny", "Magazyn Południowy")
    transaction {
        for (namesSet in names) {
            DepotEntity.new {
                name = namesSet
            }
        }
    }
}

fun generateInstitutions() {

    val names = listOf("Muzeum Narodowe", "Muzeum Historii Sztuki", "Muzeum Sztuk Pięknych", "Muzeum Naturalne", "Muzeum Sztuki Powszechnej", "Muzeum Dziedzictwa Narodowego", "Muzeum Sztuk Wyzwolonych", "Muzeum Sztuk Wchodnich", "Muzeum Epok Dawnych", "Muzeum Historii Pięknej")
    val cities = mutableSetOf<String>()
    while (cities.size < names.size) {
        cities.add(FAKER.address.city())
    }
    val citiesDifferent = cities.toTypedArray()

    transaction {
        for ((index, nameSet) in names.withIndex()) {
            InstitutionEntity.new {
                name = nameSet
                city = citiesDifferent[index]
            }
        }
    }
}

fun generateGalleries() {

    val names = listOf("Galeria Mała", "Galeria Królewska", "Galeria Starożytna", "Galeria Sztuki Współczesnej", "Galeria Sztuki Starożytnej", "Galeria Sztuk Pięknych", "Galeria Okrągła", "Galeria Wysoka", "Galeria Niska", "Galeria Dawna")
    transaction {
        for (nameSet in names) {
            GalleryEntity.new {
                name = nameSet
            }
        }
    }
}

fun generateStoreCase() = listOf("Renowacja", "Potrzeba odrestaurowania", "Naprawa", "Poważna naprawa", "Niewielka usterka", "Nieduże uszkodzenie", "Poważne uszkodznie", "Drobna naprawa", "Małe zainteresowanie", "Zbyt długa ekspozycja").random()

fun randomDate(startYear: Int = 1990, endYear: Int = 2000): LocalDate {
    val day = (1..28).random()
    val month = (1..12).random()
    val year = (startYear..endYear).random()
    return LocalDate.of(year, month, day)
}

fun generateHistory() {

    var idsValues = transaction { ExhibitEntity.all().map { Pair(it.valuePln, it.id.value) } }
    idsValues = idsValues.sortedByDescending { (p, _) -> p }
    val bestPrice = idsValues.first().first
    val idsCanHire = idsValues.map { (value, id) -> Pair(value != bestPrice, id) }
    val HIRE_LIMIT = 30
    println("There are ${idsCanHire.size} ids")
    idsCanHire.forEach { (canHire, id) ->
        var sumHire: Long = 0
        println("ExhibitEntity id is $id")
        transaction {
            var dateStart = randomDate()
            val exhibit = ExhibitEntity.findById(id)!!
            while (true) {
                val days = (10..60).random().toLong()
                val dateEnd = dateStart.plusDays(days)
                if (dateEnd.isAfter(LocalDate.now())) {
                    println("Stopped after iterations")
                    break
                }
                else {
                    val EXHIBITION = 0
                    val HIRE = 1
                    val STORAGE = 2
                    when (if ((1..100).random() < 75) EXHIBITION else (if ((0..1).random() == 0 || !canHire || sumHire + days + 1 > HIRE_LIMIT) STORAGE else HIRE)) {
                        EXHIBITION -> {
                            val created = ExhibitionEntity.new {
                                roomId = RoomEntity.all().toList().random().id
                            }
                            EventEntity.new {
                                exhibitId = exhibit.id
                                storageId = null
                                hireId = null
                                exhibitionId = created.id
                                dateFrom = dateStart
                                dateTo = dateEnd
                            }
                        }
                        HIRE -> {
                            val created = HireEntity.new {
                                institutionId = InstitutionEntity.all().toList().random().id
                            }
                            EventEntity.new {
                                exhibitId = exhibit.id
                                storageId = null
                                hireId = created.id
                                exhibitionId = null
                                dateFrom = dateStart
                                dateTo = dateEnd
                            }
                            sumHire += days
                        }
                        STORAGE -> {
                            val created = StorageEntity.new {
                                depotId = DepotEntity.all().toList().random().id
                                storeCase = generateStoreCase()
                            }
                            EventEntity.new {
                                exhibitId = exhibit.id
                                storageId = created.id
                                hireId = null
                                exhibitionId = null
                                dateFrom = dateStart
                                dateTo = dateEnd
                            }
                        }
                    }
                }
                if (dateEnd.year != dateEnd.year) {
                    sumHire = 0
                }
                dateStart = dateEnd.plusDays(1)
            }
        }
    }
}

fun generateRooms(clearAll: Boolean = true, inGalleryMin: Int = 5, inGalleryMax: Int = 10) {
    if (clearAll) {
        transaction { RoomEntity.all().forEach { it.delete() } }
    }

    transaction {
        GalleryEntity.all().forEach {
            val id = it.id
            repeat((inGalleryMin..inGalleryMax).random()) {
                RoomEntity.new {
                    areaM = (50..300).random().toDouble()
                    galleryId = id
                }
            }
        }
    }
}

fun main() {
    DbSettings.connect

    generateInstitutions()
    generateDepots()
    generateGalleries()
    generateRooms()
    generateArtists()
    generateExhibits()
    generateHistory()
}