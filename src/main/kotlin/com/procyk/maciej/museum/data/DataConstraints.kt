package com.procyk.maciej.museum.data

object DataConstraints {
    const val MAX_DEPOT_NAME_LENGTH = 255
    const val MAX_INSTITUTION_NAME_LENGTH = 255
    const val MAX_INSTITUTION_CITY_LENGTH = 255
    const val MAX_GALLERY_NAME_LENGTH = 255
    const val MAX_ARTIST_NAME_LENGTH = 255
    const val MAX_ARTIST_SURNAME_LENGTH = 255

    const val MAX_EXHIBIT_TITLE_LENGTH = 255
    const val MAX_EXHIBIT_TYPE_LENGTH = 127

    const val MAX_STORE_CASE_LENGTH = 511

    const val MAX_WIDTH_CM = 1_000
    const val MAX_HEIGHT_CM = 1_000
    const val MAX_WEIGHT_G = 100_000
    const val MAX_VALUE_PLN = 1_000_000
    const val MAX_ROOM_AREA = 500.0
}