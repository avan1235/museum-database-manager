package com.procyk.maciej.museum.data

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import tornadofx.FX.Companion.messages

class InstitutionData(id: Int?, name: String, city: String) : AbstractIdData() {
    companion object {
        fun getEmpty() = InstitutionData(null, "", "")
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty(id)
    override val id by idProperty

    val nameProperty = SimpleStringProperty(name)
    var name by nameProperty

    val cityProperty = SimpleStringProperty(city)
    var city by cityProperty

    override fun toString() = "${messages["Institution"]} \"$name\" ${messages["from"]} $city"

    fun copy(id: Int? = this.id, name: String = this.name, city: String = this.city) = InstitutionData(id, name, city)
}