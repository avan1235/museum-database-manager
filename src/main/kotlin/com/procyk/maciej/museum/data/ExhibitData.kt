package com.procyk.maciej.museum.data

import javafx.beans.property.SimpleIntegerProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import tornadofx.FX.Companion.messages

class ExhibitData(id: Int?, title: String, type:String,
                  heightCm: Int, widthCm: Int, weightG: Int,
                  artist: ArtistData?, valuePln: Int) : AbstractIdData() {

    companion object {
        fun getEmpty() = ExhibitData(null, "", "", 0, 0, 0, ArtistData.getEmpty(), 0)
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty<Int>(id)
    override val id: Int? by idProperty

    val titleProperty = SimpleStringProperty(title)
    var title by titleProperty

    val typeProperty = SimpleStringProperty(type)
    var type by typeProperty

    val heightCmProperty = SimpleIntegerProperty(heightCm)
    var heightCm by heightCmProperty

    val widthCmProperty = SimpleIntegerProperty(widthCm)
    var widthCm by widthCmProperty

    val weightGProperty = SimpleIntegerProperty(weightG)
    var weightG by weightGProperty

    val artistProperty = SimpleObjectProperty<ArtistData>(artist)
    var artist: ArtistData? by artistProperty

    val valuePlnProperty = SimpleIntegerProperty(valuePln)
    var valuePln by valuePlnProperty

    override fun toString() = "\"$title\"${if (artist != null) " ${messages["created by"]} $artist" else " ${messages["no artist"]}"}"

    fun copy(id: Int? = this.id, title: String = this.title, type:String = this.type,
             heightCm: Int = this.heightCm, widthCm: Int = this.widthCm, weightG: Int = this.weightG,
             artist: ArtistData? = this.artist?.copy(), valuePln: Int = this.valuePln) = ExhibitData(id, title, type, heightCm, widthCm, weightG, artist, valuePln)
}