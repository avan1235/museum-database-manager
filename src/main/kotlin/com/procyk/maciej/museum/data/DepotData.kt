package com.procyk.maciej.museum.data

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import tornadofx.FX.Companion.messages

class DepotData(id: Int?, name: String) : AbstractIdData() {

    companion object {
        fun getEmpty() = DepotData(null, "")
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty(id)
    override val id by idProperty

    val nameProperty = SimpleStringProperty(name)
    var name by nameProperty

    override fun toString() = "${messages["Depot"]} \"$name\""

    fun copy(id: Int? = this.id, name: String = this.name) = DepotData(id, name)
}