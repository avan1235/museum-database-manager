package com.procyk.maciej.museum.data

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import tornadofx.*
import tornadofx.FX.Companion.messages
import java.time.LocalDate
import java.time.temporal.ChronoUnit

class EventData(id: Int?, describedEventData: DescribedEventData, exhibit: ExhibitData, dateFrom: LocalDate, dateTo: LocalDate) : AbstractIdData() {

    companion object {
        fun getEmpty() = EventData(null, ExhibitionEventData.getEmpty(), ExhibitData.getEmpty(), LocalDate.now(), LocalDate.now())
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty(id)
    override val id by idProperty

    val exhibitProperty = SimpleObjectProperty<ExhibitData>(exhibit)
    var exhibit by exhibitProperty

    val dateFromProperty = SimpleObjectProperty<LocalDate>(dateFrom)
    var dateFrom by dateFromProperty

    val dateToProperty = SimpleObjectProperty<LocalDate>(dateTo)
    var dateTo by dateToProperty

    val describedEventDataProperty = SimpleObjectProperty(describedEventData)
    val describedEventData by describedEventDataProperty

    val length: Long
        get() { return dateFrom.until(dateTo, ChronoUnit.DAYS) + 1 }

    fun copy(id: Int? = this.id, describedEventData: DescribedEventData = this.describedEventData.copy(), exhibit: ExhibitData = this.exhibit.copy(), dateFrom: LocalDate = this.dateFrom, dateTo: LocalDate = this.dateTo)
            = EventData(id, describedEventData, exhibit, dateFrom, dateTo)
}

interface DescribedEventData {
    val description: String
    fun copy(): DescribedEventData
}

class StoreEventData(id: Int?, storeCase: String, depotData: DepotData) : DescribedEventData, AbstractIdData() {

    companion object {
        fun getEmpty() = StoreEventData(null, "", DepotData.getEmpty())
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty(id)
    override val id by idProperty

    val storeCaseProperty = SimpleStringProperty(storeCase)
    var storeCase by storeCaseProperty

    val depotDataProperty = SimpleObjectProperty(depotData)
    var depotData by depotDataProperty

    override val description = "${messages["Stored in"]} ${depotData.name} ${messages["because"]} $storeCase"

    fun copyDetailed(id: Int? = this.id, storeCase: String = this.storeCase, depotData: DepotData=this.depotData.copy()) = StoreEventData(id, storeCase, depotData)

    override fun copy(): DescribedEventData = this.copyDetailed()
}

class HireEventData(id: Int?, institutionData: InstitutionData) : DescribedEventData, AbstractIdData() {

    companion object {
        fun getEmpty() = HireEventData(null, InstitutionData.getEmpty())
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty(id)
    override val id by idProperty

    val  institutionDataProperty = SimpleObjectProperty(institutionData)
    var institutionData by institutionDataProperty

    override val description = "${messages["Hired for"]} ${institutionData.name}"

    fun copyDetailed(id: Int? = this.id, institutionData: InstitutionData = this.institutionData.copy()) = HireEventData(id, institutionData)

    override fun copy(): DescribedEventData = this.copyDetailed()
}

class ExhibitionEventData(id: Int?, roomData: RoomData) : DescribedEventData, AbstractIdData() {

    companion object {
        fun getEmpty() = ExhibitionEventData(null, RoomData.getEmpty())
        val EMPTY = getEmpty()
    }

    val idProperty = SimpleObjectProperty(id)
    override val id by idProperty

    val roomDataProperty = SimpleObjectProperty(roomData)
    var roomData by roomDataProperty

    override val description = "${messages["Exhibition in"]} $roomData"

    fun copyDetailed(id: Int? = this.id, roomData: RoomData = this.roomData.copy()) = ExhibitionEventData(id, roomData)

    override fun copy(): DescribedEventData = this.copyDetailed()
}