package com.procyk.maciej.museum.view.screen

import com.procyk.maciej.museum.controller.ArtistController
import com.procyk.maciej.museum.controller.ExhibitController
import com.procyk.maciej.museum.data.ArtistData
import com.procyk.maciej.museum.data.DataConstraints
import com.procyk.maciej.museum.data.ExhibitData
import com.procyk.maciej.museum.view.*
import com.procyk.maciej.museum.view.icon.Icons
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.geometry.Insets
import javafx.scene.control.ComboBox
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.Priority
import javafx.stage.Modality
import javafx.stage.StageStyle
import javafx.stage.Window
import tornadofx.*
import tornadofx.FX.Companion.messages

class ExhibitEditScreen : View(messages["Edit Exhibit"]) {

    private val exhibitController: ExhibitController by inject()
    private val artistController: ArtistController by inject()

    private val isExistingArtist = SimpleBooleanProperty()

    private var artistSelectField: Field? = null
    private var artistCreateField: Field? = null
    private var artistCombobox: ComboBox<ArtistData>? = null

    private val valueLSB: SimpleIntegerProperty = SimpleIntegerProperty()
    private val valueMSB: SimpleIntegerProperty = SimpleIntegerProperty()

    private lateinit var editedExhibitData: ExhibitData
    private var originalArtistData: ArtistData? = null

    init {
        primaryStage.icons += Icons.APP_ICON.image
    }

    fun reloadAndOpenWindow(
            exhibitData: ExhibitData,
            stageStyle: StageStyle = StageStyle.DECORATED,
            modality: Modality = Modality.WINDOW_MODAL,
            escapeClosesWindow: Boolean = true,
            owner: Window? = currentWindow,
            block: Boolean = false,
            resizable: Boolean? = false) {

        editedExhibitData = exhibitData
        valueLSB.value = exhibitData.valuePln % 1000
        valueMSB.value = exhibitData.valuePln / 1000
        originalArtistData = exhibitData.artist
        isExistingArtist.set(originalArtistData != null)
        root.center = getCenterRefreshed()

        isExistingArtist.onChange { exists ->
            if (exists) {
                artistSelectField?.show()
                artistCreateField?.hide()
                editedExhibitData.artist = originalArtistData
                artistCombobox?.selectionModel?.select(editedExhibitData.artist)
            }
            else {
                artistSelectField?.hide()
                artistCreateField?.show()
            }
            this.currentStage?.sizeToScene()
        }

        valueLSB.onChange { value ->
            val newValue = 1000 * valueMSB.value + value
            editedExhibitData.valuePln = if (newValue > 0) newValue else 1
        }
        valueMSB.onChange { value ->
            editedExhibitData.valuePln = 1000 * value + valueLSB.value
        }
        editedExhibitData.valuePlnProperty.onChange { value ->
            valueLSB.value = value % 1000
            valueMSB.value = value / 1000
        }

        openWindow(stageStyle, modality, escapeClosesWindow, owner, block, resizable)
    }

    override val root = borderpane()

    private fun getCenterRefreshed() =
            form {
                fieldset(messages["Basic information"]) {
                    field(messages["Title"]) {
                        textfield(editedExhibitData.titleProperty) {
                            maxLength(DataConstraints.MAX_EXHIBIT_TITLE_LENGTH)
                        }
                    }

                    field(messages["Type"]) {
                        textfield(editedExhibitData.typeProperty) {
                            maxLength(DataConstraints.MAX_EXHIBIT_TYPE_LENGTH)
                        }
                    }
                }

                fieldset(messages["Sizes"]) {
                    field("${messages["Height"]} [cm]") {
                        positiveSpinner(editedExhibitData.heightCmProperty, max = DataConstraints.MAX_HEIGHT_CM)
                        positiveSlider(DataConstraints.MAX_HEIGHT_CM).bind(editedExhibitData.heightCmProperty)
                    }

                    field("${messages["Width"]} [cm]") {
                        positiveSpinner(editedExhibitData.widthCmProperty, max = DataConstraints.MAX_WIDTH_CM)
                        positiveSlider(DataConstraints.MAX_WIDTH_CM).bind(editedExhibitData.widthCmProperty)
                    }

                    field("${messages["Weight"]} [g]") {
                        positiveSpinner(editedExhibitData.weightGProperty, max = DataConstraints.MAX_WEIGHT_G)
                        positiveSlider(DataConstraints.MAX_WEIGHT_G).bind(editedExhibitData.weightGProperty)
                    }
                }

                fieldset(messages["Other"]) {
                    field("${messages["Value"]} [pln]") {
                        hbox {
                            positiveSpinner(editedExhibitData.valuePlnProperty, max = DataConstraints.MAX_VALUE_PLN)
                            vbox(spacing = 5.0) {
                                padding = Insets(0.0, 0.0, 0.0, 10.0)
                                positiveSlider(1000).bind(valueLSB)
                                positiveSlider(DataConstraints.MAX_VALUE_PLN / 1000, includeZero = true).bind(valueMSB)
                            }
                        }
                    }

                    field(messages["Artist"]) {
                        checkbox(messages["Existing artist"], isExistingArtist) {
                            hboxConstraints { margin = Insets(3.0) }
                            vboxConstraints { margin = Insets(3.0) }
                        }
                    }

                    field(messages["Select"]) {
                        artistSelectField =  this
                        combobox<ArtistData>(editedExhibitData.artistProperty, artistController.getPossibleArtists()) {
                            artistCombobox = this
                            selectionModel.select(editedExhibitData.artist)
                        }
                    }

                    field(messages["Create"]) {
                        artistCreateField = this
                        artistForm(ArtistData.getEmpty(), this@ExhibitEditScreen, editedExhibitData, isExistingArtist)
                        hide()
                    }
                }

                saveCancelButtons(
                        { exhibitController.saveEdited(editedExhibitData) },
                        { exhibitController.closeExhibitEditScreen() })
            }
}