package com.procyk.maciej.museum.view.screen

import com.procyk.maciej.museum.controller.EventController
import com.procyk.maciej.museum.data.DescribedEventData
import com.procyk.maciej.museum.data.EventData
import com.procyk.maciej.museum.view.cancelButton
import com.procyk.maciej.museum.view.eventViewTable
import com.procyk.maciej.museum.view.proceedButton
import com.procyk.maciej.museum.view.smallTitleLabel
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.layout.Priority
import javafx.stage.Modality
import javafx.stage.StageStyle
import javafx.stage.Window
import tornadofx.*

class EventWarningScreen : View(FX.messages["Changes Warning"]) {

    private val eventController: EventController by inject()

    private lateinit var fullyDeletedData: List<EventData>
    private lateinit var partiallyChangedData: Pair<List<EventData>, List<EventData>>
    private lateinit var theSameData: List<EventData>
    private lateinit var separatedData: List<EventData>

    private lateinit var describedEventData: DescribedEventData
    private lateinit var editedEventData: EventData

    private lateinit var onClose: () -> Unit

    override val root = borderpane()

    fun reloadAndOpenWindow(fullyDeletedData: List<EventData>, partiallyChangedData: Pair<List<EventData>, List<EventData>>,
                            theSameData: List<EventData>, separatedData: List<EventData>,
                            editedEventData: EventData, describedEventData: DescribedEventData,
                            onClose: () -> Unit = {},
                            stageStyle: StageStyle = StageStyle.DECORATED,
                            modality: Modality = Modality.WINDOW_MODAL,
                            escapeClosesWindow: Boolean = true,
                            owner: Window? = currentWindow,
                            block: Boolean = false,
                            resizable: Boolean? = false) {

        this.fullyDeletedData = fullyDeletedData
        this.partiallyChangedData = partiallyChangedData
        this.separatedData = separatedData
        this.theSameData = theSameData
        this.onClose = onClose
        this.editedEventData = editedEventData
        this.describedEventData = describedEventData

        root.center = getCenterRefreshed()
        openWindow(stageStyle, modality, escapeClosesWindow, owner, block, resizable)
    }

    private fun getCenterRefreshed() = vbox {
        setMaxSize(800.0, 600.0)
        hbox {
            vbox {
                paddingAll = 7.0
                smallTitleLabel(messages["Fully deleted events"], fontSize = 13.0) {
                    hboxConstraints { margin = Insets(0.0, 0.0, 0.0, 10.0) }
                }
                eventViewTable(fullyDeletedData.observable(), hideExhibit = true) {
                    hgrow = Priority.ALWAYS
                    vgrow = Priority.ALWAYS
                }
            }
            vbox {
                paddingAll = 7.0
                smallTitleLabel(messages["Partially changed events"], fontSize = 13.0) {
                    hboxConstraints { margin = Insets(0.0, 0.0, 0.0, 10.0) }
                }
                eventViewTable(listOf(partiallyChangedData.first, partiallyChangedData.second).flatMap { it.asIterable() } .observable(), hideExhibit = true) {
                    hgrow = Priority.ALWAYS
                    vgrow = Priority.ALWAYS
                }
            }
        }
        hbox {
            vbox {
                paddingAll = 7.0
                smallTitleLabel(messages["The same length events"], fontSize = 13.0) {
                    hboxConstraints { margin = Insets(0.0, 0.0, 0.0, 10.0) }
                }
                eventViewTable(theSameData.observable(), hideExhibit = true) {
                    hgrow = Priority.ALWAYS
                    vgrow = Priority.ALWAYS
                }
            }
            vbox {
                paddingAll = 7.0
                smallTitleLabel(messages["Separated events"], fontSize = 13.0) {
                    hboxConstraints { margin = Insets(0.0, 0.0, 0.0, 10.0) }
                }
                eventViewTable(separatedData.observable(), hideExhibit = true) {
                    hgrow = Priority.ALWAYS
                    vgrow = Priority.ALWAYS
                }
            }
        }
        hbox {
            hgrow = Priority.ALWAYS
            alignment = Pos.CENTER
            paddingAll = 7.0
            proceedButton {
                eventController.proceedWithChangesFor(editedEventData, describedEventData, fullyDeletedData, partiallyChangedData, theSameData, separatedData)
                isDisable = true
            }
            cancelButton {
                eventController.closeEventWarningScreen()
            }
        }
    }
}