package com.procyk.maciej.museum.view.screen

import com.procyk.maciej.museum.app.Language
import com.procyk.maciej.museum.controller.*
import com.procyk.maciej.museum.data.*
import com.procyk.maciej.museum.view.*
import com.procyk.maciej.museum.view.icon.Icons
import com.procyk.maciej.museum.view.table.ArtistViewTable
import com.procyk.maciej.museum.view.table.EventViewTable
import com.procyk.maciej.museum.view.table.ExhibitViewTable
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.ComboBox
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.layout.*
import tornadofx.*
import tornadofx.FX.Companion.messages
import tornadofx.controlsfx.toggleswitch
import java.lang.Integer.max
import java.time.LocalDate

class AppScreen : View(messages["Museum Knowledge Base"]) {

    private val loginController: LoginController by inject()
    private val artistController: ArtistController by inject()
    private val exhibitController: ExhibitController by inject()
    private val eventController: EventController by inject()
    private val othersController: OthersController by inject()
    private val languageController: LanguageController by inject()

    private lateinit var tabPane: TabPane
    private lateinit var othersTab: Tab
    private lateinit var exhibitViewTable: ExhibitViewTable
    private lateinit var artistViewTable: ArtistViewTable
    private lateinit var eventViewTable: EventViewTable

    // filters for exhibits
    private val isFilterExhibitsNames = SimpleBooleanProperty()
    private val filterExhibitsNames = SimpleStringProperty()
    private val isFilterExhibitsArtists = SimpleBooleanProperty()
    private val filterExhibitsArtists = SimpleStringProperty()
    private val isFilterExhibitsType = SimpleBooleanProperty()
    private val filterExhibitsType = SimpleStringProperty()
    private val isFilterExhibitsValue = SimpleBooleanProperty()
    private val filterExhibitsValueFrom = SimpleDoubleProperty()
    private val filterExhibitsValueTo = SimpleDoubleProperty()
    private val isFilterExhibitsSize = SimpleBooleanProperty()
    private val filterExhibitsSizeFrom = SimpleDoubleProperty()
    private val filterExhibitsSizeTo = SimpleDoubleProperty()
    private val isFilterExhibitsWeight = SimpleBooleanProperty()
    private val filterExhibitsWeightFrom = SimpleDoubleProperty()
    private val filterExhibitsWeightTo = SimpleDoubleProperty()

    // filters for artists
    private val filterArtistsYearFrom = SimpleDoubleProperty()
    private val filterArtistsYearTo = SimpleDoubleProperty()
    private val isFilterArtistsDates = SimpleBooleanProperty()
    private val filterArtistsAlive = SimpleBooleanProperty()
    private val filterArtistsNotAlive = SimpleBooleanProperty()
    private val isFilterArtistsName = SimpleBooleanProperty()
    private val filterArtistsName = SimpleStringProperty()
    private val isFilterArtistsSurname = SimpleBooleanProperty()
    private val filterArtistsSurname = SimpleStringProperty()

    // filters for event history of exhibits
    private val filterEventsDates = SimpleBooleanProperty()
    private val filterEventsFrom = SimpleObjectProperty(LocalDate.now())
    private val filterEventsTo = SimpleObjectProperty(LocalDate.now())
    private val showExhibitions = SimpleBooleanProperty()
    private val showHires = SimpleBooleanProperty()
    private val showStorages = SimpleBooleanProperty()

    private val editContent = mutableListOf<Node>()

    val tableExhibitsData = mutableListOf<ExhibitData>().observable()
    val fullExhibitsData = mutableListOf<ExhibitData>().observable()

    val tableArtistsData = mutableListOf<ArtistData>().observable()
    val fullArtistsData = mutableListOf<ArtistData>().observable()

    val tableEventData = mutableListOf<EventData>().observable()
    val fullEventData = mutableListOf<EventData>().observable()

    private val editedDepotData: DepotData = DepotData.getEmpty()
    private val editedInstitutionData: InstitutionData = InstitutionData.getEmpty()
    private val editedGalleryData: GalleryData = GalleryData.getEmpty()
    private val editedRoomData: RoomData = RoomData.getEmpty()

    private val isEditMode = SimpleBooleanProperty(false)
    private val language = SimpleObjectProperty<Language>(Language.DEFAULT_LANGUAGE)

    init {
        primaryStage.icons += Icons.APP_ICON.image

        initEventFilters()
        initArtistFilters()
        initExhibitsFilters()

        fullEventData.onChange { tableEventData.setAll(fullEventData) }
        fullArtistsData.onChange { tableArtistsData.setAll(fullArtistsData) }
        fullExhibitsData.onChange { tableExhibitsData.setAll(fullExhibitsData) }

        listOf(filterArtistsYearFrom, filterArtistsYearTo, isFilterArtistsDates, filterArtistsAlive, filterArtistsNotAlive,
                isFilterArtistsName, isFilterArtistsSurname, filterArtistsName, filterArtistsSurname)
                .forEach { it.addListener { _, _, _ -> updateArtistsFilters() } }

        listOf(filterExhibitsNames, isFilterExhibitsNames, filterExhibitsArtists, isFilterExhibitsArtists,
                filterExhibitsType, isFilterExhibitsType, filterExhibitsValueFrom, filterExhibitsValueTo, isFilterExhibitsValue,
                isFilterExhibitsSize, filterExhibitsSizeFrom, filterExhibitsSizeTo,
                isFilterExhibitsWeight, filterExhibitsWeightFrom, filterExhibitsWeightTo)
                .forEach { it.addListener {_, _, _ -> updateExhibitsFilters() } }
    }

    override val root = borderpane {
        primaryStage.isResizable = true
        setPrefSize(1200.0, 700.0)
    }

    fun reloadWindow() {

        initEventFilters()
        initArtistFilters()
        initExhibitsFilters()

        root.top = getTopRefreshed()
        root.center = getCenterRefreshed()
        isEditMode.onChange { canEdit ->
            editContent.forEach { node -> if (canEdit) node.show() else node.hide() }
            if (canEdit) tabPane.tabs.add(othersTab) else tabPane.tabs.remove(othersTab)
        }
        isEditMode.value = loginController.isLoggedMod()
    }

    private fun initEventFilters() {
        showExhibitions.value = true
        showHires.value = true
        showStorages.value = true
        filterEventsDates.value = false
        filterEventsFrom.value = LocalDate.now()
        filterEventsTo.value = LocalDate.now()

        arrayOf(showExhibitions, showStorages, showHires).forEach { it.onChange {
            updateHistoryFilters()
        } }
    }

    private fun initArtistFilters() {
        isFilterArtistsDates.value = false
        filterArtistsYearFrom.value = 1.0
        filterArtistsYearTo.value = LocalDate.now().year.toDouble()

        filterArtistsNotAlive.value = true
        filterArtistsAlive.value = true

        isFilterArtistsName.value = false
        isFilterArtistsSurname.value = false
        filterArtistsName.value = ""
        filterArtistsSurname.value = ""
    }

    private fun initExhibitsFilters() {
        isFilterExhibitsArtists.value = false
        isFilterExhibitsNames.value = false
        isFilterExhibitsType.value = false
        isFilterExhibitsValue.value = false
        isFilterExhibitsSize.value = false
        isFilterExhibitsWeight.value = false

        filterExhibitsArtists.value = ""
        filterExhibitsNames.value = ""
        filterExhibitsType.value = ""
        filterExhibitsValueFrom.value = 1.0
        filterExhibitsValueTo.value = DataConstraints.MAX_VALUE_PLN.toDouble()
        filterExhibitsSizeFrom.value = 1.0
        filterExhibitsSizeTo.value = max(DataConstraints.MAX_HEIGHT_CM, DataConstraints.MAX_WIDTH_CM).toDouble()
        filterExhibitsWeightFrom.value = 1.0
        filterExhibitsWeightTo.value = DataConstraints.MAX_WEIGHT_G.toDouble()
    }

    private fun getCenterRefreshed() =
            tabpane {
                tabPane = this
                tabMinWidth = 100.0

                notClosableTab(messages["Exhibits"]) {
                    hbox {
                        vbox {
                            paddingAll = 10.0
                            smallTitleLabel(FX.messages["Filters"])
                            vbox {
                                filterStrings(messages["Filter exhibits names"], isFilterExhibitsNames, filterExhibitsNames)
                                filterStrings(messages["Filter exhibits artists"], isFilterExhibitsArtists, filterExhibitsArtists)
                                filterStrings(messages["Filter exhibits type"], isFilterExhibitsType, filterExhibitsType)
                                filterIntRange("${messages["Filter value"]} [pln]", filterExhibitsValueFrom, filterExhibitsValueTo, isFilterExhibitsValue, 1, DataConstraints.MAX_VALUE_PLN)
                                filterIntRange("${messages["Filter size"]} [cm]", filterExhibitsSizeFrom, filterExhibitsSizeTo, isFilterExhibitsSize, 1, max(DataConstraints.MAX_WIDTH_CM, DataConstraints.MAX_HEIGHT_CM))
                                filterIntRange("${messages["Filter weight"]} [g]", filterExhibitsWeightFrom, filterExhibitsWeightTo, isFilterExhibitsWeight, 1, DataConstraints.MAX_WEIGHT_G)
                            }
                        }

                        exhibitViewTable(tableExhibitsData) {
                            hgrow = Priority.ALWAYS
                            vgrow = Priority.ALWAYS
                            exhibitViewTable =  this
                            onDoubleClick {
                                eventController.openHistoryEventLogWindow(exhibitViewTable.selectedItem, isEditMode.value)
                            }
                        }

                        vbox {
                            paddingAll = 10.0

                            buttonMargin(messages["Refresh data"]) {
                                setOnAction {
                                    exhibitController.refreshExhibitData(fullExhibitsData)
                                    initExhibitsFilters()
                                }
                            }

                            buttonMargin(messages["Edit selected"]) {
                                setOnAction {
                                    exhibitController.openEditWindow(exhibitViewTable.selectedItem)
                                }
                                makeHideableForMod()
                            }

                            buttonMargin(messages["Create new"]) {
                                setOnAction {
                                    exhibitController.openCreateWindow()
                                }
                                makeHideableForMod()
                            }

                            buttonMargin(messages["Remove selected"]) {
                                setOnAction {
                                    exhibitController.deleteExhibitWithHistory(exhibitViewTable.selectedItem)
                                    initExhibitsFilters()
                                }
                                makeHideableForMod()
                            }

                            buttonMargin(messages["History"]) {
                                setOnAction {
                                    eventController.openHistoryEventLogWindow(exhibitViewTable.selectedItem, isEditMode.value)
                                }
                            }
                        }
                    }
                }

                notClosableTab(messages["Artists"]) {
                    hbox {
                        vbox {
                            paddingAll = 10.0
                            smallTitleLabel(FX.messages["Filters"])
                            vbox(spacing = 10.0) {
                                filterIntRange(FX.messages["Time range"], filterArtistsYearFrom, filterArtistsYearTo, isFilterArtistsDates, 1, LocalDate.now().year)
                                toggleswitch(messages["Alive"], filterArtistsAlive)
                                toggleswitch(messages["Not alive"], filterArtistsNotAlive)
                                filterStrings(messages["Filter names"], isFilterArtistsName, filterArtistsName)
                                filterStrings(messages["Filter surnames"], isFilterArtistsSurname, filterArtistsSurname)
                            }
                        }

                        artistViewTable(tableArtistsData) {
                            artistViewTable = this
                            hgrow = Priority.ALWAYS
                            vgrow = Priority.ALWAYS
                            onDoubleClick {
                                exhibitController.openArtistExhibitsWindow(artistViewTable.selectedItem, isEditMode.value)
                            }
                        }

                        vbox {
                            paddingAll = 10.0

                            buttonMargin(messages["Refresh data"]) {
                                setOnAction {
                                    artistController.refreshArtistsData(fullArtistsData)
                                    initArtistFilters()
                                }
                            }

                            buttonMargin(messages["Edit selected"]) {
                                setOnAction {
                                    artistController.openEditWindow(artistViewTable.selectedItem)
                                }
                                makeHideableForMod()
                            }

                            buttonMargin(messages["Remove selected"]) {
                                setOnAction {
                                    artistController.deleteArtistWithExhibitsAndHistory(artistViewTable.selectedItem)
                                    initArtistFilters()
                                }
                                makeHideableForMod()
                            }
                        }
                    }
                }

                notClosableTab(messages["History"]) {
                    hbox {
                        vbox {
                            paddingAll = 10.0
                            smallTitleLabel(FX.messages["Filters"])
                            dateFilterRange(this@AppScreen::updateHistoryFilters, filterEventsDates, filterEventsFrom, filterEventsTo) {
                                paddingAll = 10.0
                            }
                            vbox(spacing = 10.0) {
                                toggleswitch(messages["Show exhibitions"], showExhibitions)
                                toggleswitch(messages["Show hires"], showHires)
                                toggleswitch(messages["Show storages"], showStorages)
                            }
                        }

                        eventViewTable(tableEventData) {
                            eventViewTable = this
                            hgrow = Priority.ALWAYS
                            vgrow = Priority.ALWAYS
                        }
                        vbox {
                            paddingAll = 10.0

                            buttonMargin(messages["Refresh data"]) {
                                setOnAction {
                                    eventController.refreshEventData(fullEventData)
                                    initEventFilters()
                                }
                            }

                            buttonMargin(messages["Edit selected"]) {
                                setOnAction {
                                    eventController.openEditWindow(eventViewTable.selectedItem)
                                }
                                makeHideableForMod()
                            }

                            buttonMargin(messages["Remove selected"]) {
                                setOnAction {
                                    eventController.deleteHistoryEvent(eventViewTable.selectedItem)
                                    initEventFilters()
                                }
                                makeHideableForMod()
                            }
                        }
                    }
                }
                notClosableTab(messages["Others"]) {
                    othersTab = this
                    vbox {
                        hbox {
                            paddingAll = 20.0

                            form {
                                borderSeparation()
                                smallTitleLabel(messages["Depot"])

                                fieldset {
                                    field(messages["Name"]) {
                                        textfield(editedDepotData.nameProperty) {
                                            maxLength(DataConstraints.MAX_DEPOT_NAME_LENGTH)
                                        }
                                    }
                                }

                                createButton {
                                    othersController.saveCreated(editedDepotData)
                                    editedDepotData.name = DepotData.EMPTY.name
                                }
                            }

                            form {
                                borderSeparation()
                                smallTitleLabel(messages["Institution"])

                                fieldset {
                                    field(messages["Name"]) {
                                        textfield(editedInstitutionData.nameProperty) {
                                            maxLength(DataConstraints.MAX_INSTITUTION_NAME_LENGTH)
                                        }
                                    }
                                    field(messages["City"]) {
                                        textfield(editedInstitutionData.cityProperty) {
                                            maxLength(DataConstraints.MAX_INSTITUTION_CITY_LENGTH)
                                        }
                                    }
                                }

                                createButton {
                                    othersController.saveCreated(editedInstitutionData)
                                    editedInstitutionData.name = InstitutionData.EMPTY.name
                                    editedInstitutionData.city = InstitutionData.EMPTY.city
                                }
                            }
                        }
                        hbox {
                            paddingAll = 20.0

                            form {
                                borderSeparation()
                                smallTitleLabel(messages["Gallery"])

                                fieldset {
                                    field(messages["Name"]) {
                                        textfield(editedGalleryData.nameProperty) {
                                            maxLength(DataConstraints.MAX_GALLERY_NAME_LENGTH)
                                        }
                                    }
                                }

                                createButton {
                                    othersController.saveCreated(editedGalleryData)
                                    editedGalleryData.name = GalleryData.EMPTY.name
                                }
                            }

                            form {
                                borderSeparation()
                                smallTitleLabel(messages["Room"])

                                fieldset {
                                    field("${messages["Area"]} [m\u00B2]") {
                                        textfield(editedRoomData.areaMProperty) {
                                            isEditable = false
                                        }
                                        positiveSlider(DataConstraints.MAX_ROOM_AREA, editedRoomData.areaM) {
                                            blockIncrement = 1.0
                                            bind(editedRoomData.areaMProperty)
                                        }
                                    }
                                    hbox {
                                        var combobox: ComboBox<GalleryData>? = null
                                        field(messages["Gallery"]) {
                                            combobox<GalleryData>(editedRoomData.galleryDataProperty, othersController.getPossibleGalleries()) {
                                                editedRoomData.galleryDataProperty.addListener { _, _, newValue -> if (newValue == GalleryData.EMPTY) selectionModel.selectFirst() }
                                                selectionModel.selectFirst()
                                                combobox = this
                                            }
                                        }
                                        buttonMargin(messages["Refresh"], prefWidth = 100.0) {
                                            setOnAction {
                                                combobox?.items?.setAll(othersController.getPossibleGalleries())
                                            }
                                        }
                                    }
                                }

                                createButton {
                                    othersController.saveCreated(editedRoomData)
                                    editedRoomData.areaM = RoomData.EMPTY.areaM
                                    editedRoomData.galleryData = GalleryData.getEmpty()
                                }
                            }
                        }
                    }
                }
                if (!isEditMode.value) this.tabs.remove(othersTab)
            }

    private fun updateHistoryFilters() {
        filterEvents(filterEventsDates.value, filterEventsFrom.value, filterEventsTo.value)
    }

    private fun updateArtistsFilters() {
        filterArtists(isFilterArtistsDates.value, filterArtistsYearFrom.value.toInt(), filterArtistsYearTo.value.toInt(),
                isFilterArtistsName.value, filterArtistsName.value, isFilterArtistsSurname.value, filterArtistsSurname.value)
    }

    private fun updateExhibitsFilters() {
        filterExhibits(isFilterExhibitsNames.value, filterExhibitsNames.value,
                isFilterExhibitsArtists.value, filterExhibitsArtists.value,
                isFilterExhibitsType.value, filterExhibitsType.value,
                isFilterExhibitsValue.value, filterExhibitsValueFrom.value.toInt(), filterExhibitsValueTo.value.toInt(),
                isFilterExhibitsSize.value, filterExhibitsSizeFrom.value.toInt(), filterExhibitsSizeTo.value.toInt(),
                isFilterExhibitsWeight.value, filterExhibitsWeightFrom.value.toInt(), filterExhibitsWeightTo.value.toInt())
    }

    private fun Iterable<EventData>.filterEventsChecks(): List<EventData> {
        return this
                .filter { showExhibitions.value || !(it.describedEventData is ExhibitionEventData) }
                .filter { showHires.value || !(it.describedEventData is HireEventData) }
                .filter { showStorages.value || !(it.describedEventData is StoreEventData) }
    }

    private fun Iterable<ArtistData>.filterArtistsChecks(): List<ArtistData> {
        return this
                .filter { filterArtistsAlive.value || !it.isAlive }
                .filter { filterArtistsNotAlive.value || it.isAlive }
    }

    private fun filterEvents(filterDates: Boolean, dateFrom: LocalDate, dateTo: LocalDate) {
        tableEventData.setAll(fullEventData
                .filter { !filterDates || (!(it.dateFrom.isAfter(dateTo) || it.dateTo.isBefore(dateFrom))) }
                .filterEventsChecks())
    }

    private fun filterArtists(filterDates: Boolean, dateFrom: Int, dateTo: Int,
                              filterNames: Boolean, namePrefix: String, filterSurnames: Boolean, surnamePrefix: String) {
        tableArtistsData.setAll(fullArtistsData
                .filter { !filterDates || (!(it.birthYear > dateTo || (it?.deathYear ?: LocalDate.now().year) < dateFrom)) }
                .filter { !filterNames || it.name.startsWithIgnoreCase(namePrefix) }
                .filter { !filterSurnames || it.surname.startsWithIgnoreCase(surnamePrefix) }
                .filterArtistsChecks()
        )
    }

    private fun filterExhibits(filterNames: Boolean, namePrefix: String,
                               filterArtists: Boolean, artistsPrefix: String,
                               filterTypes: Boolean, typesPrefix: String,
                               filterValues: Boolean, valuesFrom: Int, valuesTo: Int,
                               filterSizes: Boolean, sizeFrom: Int, sizeTo: Int,
                               filterWeight: Boolean, weightFrom: Int, weightTo: Int) {
        tableExhibitsData.setAll(fullExhibitsData
                .filter { !filterArtists || it.artist?.toString()?.startsWithIgnoreCase(artistsPrefix) ?: false }
                .filter { !filterNames || it.title.startsWithIgnoreCase(namePrefix) }
                .filter { !filterTypes || it.type.startsWithIgnoreCase(typesPrefix) }
                .filter { !filterValues || (it.valuePln in valuesFrom..valuesTo) }
                .filter { !filterSizes || it.widthCm in sizeFrom..sizeTo || it.heightCm in sizeFrom..sizeTo }
                .filter { !filterWeight || it.weightG in weightFrom..weightTo }
        )
    }

    private fun getTopRefreshed() = hbox {
        hbox {
            paddingAll = 3.0
            alignment = Pos.CENTER_LEFT
            hgrow = Priority.ALWAYS

            checkbox(messages["Edit mode"], isEditMode) {
                hboxConstraints { margin = Insets(3.0) }
                vboxConstraints { margin = Insets(3.0) }
                if (!loginController.isLoggedMod()) hide()
                else isEditMode.value = true
            }

            label(messages["Change language"]) {
                hboxConstraints { margin = Insets(3.0, 3.0, 3.0, 10.0) }
                vboxConstraints { margin = Insets(3.0, 3.0, 3.0, 10.0) }
            }
            combobox(language, Language.values().toList()) {
                hboxConstraints { margin = Insets(3.0) }
                vboxConstraints { margin = Insets(3.0) }
                selectionModel.selectFirst()
            }
            buttonMargin(messages["Save and exit"]) {
                setOnAction {
                    languageController.saveToConfig(language.value)
                    Platform.exit()
                }
            }
        }
        hbox {
            paddingAll = 3.0
            alignment = Pos.CENTER_RIGHT

            buttonMargin(messages["Logout"], prefWidth = 90.0) {
                isDefaultButton = true
                setOnAction { loginController.logout() }
            }

            buttonMargin(messages["Exit"], prefWidth = 90.0) {
                setOnAction { Platform.exit() }
            }
        }
    }

    private fun Node.makeHideableForMod() {
        editContent.add(this)
        if (!loginController.isLoggedMod()) {
            hide()
        }
    }

    /**
     * The order on the TabSelected enum types has to match the order
     * of tabs created in the AppScreen View in order ro work properly
     */
    enum class TabSelected {
        Exhibits,
        Artists,
        History,
        Others
    }

    fun selectTab(tab: TabSelected) =
            when (tab) {
                TabSelected.Exhibits -> {
                    tabPane.selectionModel?.select(TabSelected.Exhibits.ordinal)
                }
                TabSelected.Artists -> {
                    tabPane.selectionModel?.select(TabSelected.Artists.ordinal)
                }
                TabSelected.History -> {
                    tabPane.selectionModel?.select(TabSelected.History.ordinal)
                }
                TabSelected.Others -> {
                    tabPane.selectionModel?.select(TabSelected.Others.ordinal)
                }
            }
}

fun String.startsWithIgnoreCase(prefix: String) = this.toLowerCase().startsWith(prefix.toLowerCase())