package com.procyk.maciej.museum.view.screen

import com.procyk.maciej.museum.controller.EventController
import com.procyk.maciej.museum.controller.ExhibitController
import com.procyk.maciej.museum.data.*
import com.procyk.maciej.museum.view.*
import com.procyk.maciej.museum.view.icon.Icons
import com.procyk.maciej.museum.view.table.ExhibitViewTable
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleDoubleProperty
import javafx.beans.property.SimpleStringProperty
import javafx.geometry.Insets
import javafx.scene.Node
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.stage.Modality
import javafx.stage.StageStyle
import javafx.stage.Window
import tornadofx.*
import tornadofx.FX.Companion.messages

class ArtistExhibitsScreen : View(messages["Artist Exhibits"]) {

    private val exhibitController: ExhibitController by inject()
    private val eventController: EventController by inject()

    private val fullExhibitsData = mutableListOf<ExhibitData>().observable()
    private val tableExhibitsData = mutableListOf<ExhibitData>().observable()
    private var loadedArtistData: ArtistData? = null
    private var editMode = false

    private lateinit var exhibitViewTable: ExhibitViewTable

    // filters for exhibits
    private val isFilterExhibitsNames = SimpleBooleanProperty()
    private val filterExhibitsNames = SimpleStringProperty()
    private val isFilterExhibitsArtists = SimpleBooleanProperty()
    private val filterExhibitsArtists = SimpleStringProperty()
    private val isFilterExhibitsType = SimpleBooleanProperty()
    private val filterExhibitsType = SimpleStringProperty()
    private val isFilterExhibitsValue = SimpleBooleanProperty()
    private val filterExhibitsValueFrom = SimpleDoubleProperty()
    private val filterExhibitsValueTo = SimpleDoubleProperty()
    private val isFilterExhibitsSize = SimpleBooleanProperty()
    private val filterExhibitsSizeFrom = SimpleDoubleProperty()
    private val filterExhibitsSizeTo = SimpleDoubleProperty()
    private val isFilterExhibitsWeight = SimpleBooleanProperty()
    private val filterExhibitsWeightFrom = SimpleDoubleProperty()
    private val filterExhibitsWeightTo = SimpleDoubleProperty()

    init {
        primaryStage.icons += Icons.APP_ICON.image
        fullExhibitsData.onChange { tableExhibitsData.setAll(fullExhibitsData) }

        initFilters()

        listOf(filterExhibitsNames, isFilterExhibitsNames, filterExhibitsArtists, isFilterExhibitsArtists,
                filterExhibitsType, isFilterExhibitsType, filterExhibitsValueFrom, filterExhibitsValueTo, isFilterExhibitsValue,
                isFilterExhibitsSize, filterExhibitsSizeFrom, filterExhibitsSizeTo,
                isFilterExhibitsWeight, filterExhibitsWeightFrom, filterExhibitsWeightTo)
                .forEach { it.addListener {_, _, _ -> updateFilters() } }
    }

    fun reloadAndOpenWindow(
            artistData: ArtistData,
            editMode: Boolean = false,
            stageStyle: StageStyle = StageStyle.DECORATED,
            modality: Modality = Modality.WINDOW_MODAL,
            escapeClosesWindow: Boolean = true,
            owner: Window? = currentWindow,
            block: Boolean = false,
            resizable: Boolean? = false) {

        initFilters()

        this.editMode = editMode
        loadedArtistData = artistData
        exhibitController.refreshEventDataForLoaded(fullExhibitsData, artistData)

        root.top = getTopRefreshed(artistData)
        root.center = getCenterRefreshed()
        root.right = getRightRefreshed()
        root.left = getLeftRefreshed()

        openWindow(stageStyle, modality, escapeClosesWindow, owner, block, resizable)
    }

    private fun initFilters() {
        isFilterExhibitsArtists.value = false
        isFilterExhibitsNames.value = false
        isFilterExhibitsType.value = false
        isFilterExhibitsValue.value = false
        isFilterExhibitsSize.value = false
        isFilterExhibitsWeight.value = false

        filterExhibitsArtists.value = ""
        filterExhibitsNames.value = ""
        filterExhibitsType.value = ""
        filterExhibitsValueFrom.value = 1.0
        filterExhibitsValueTo.value = DataConstraints.MAX_VALUE_PLN.toDouble()
        filterExhibitsSizeFrom.value = 1.0
        filterExhibitsSizeTo.value = Integer.max(DataConstraints.MAX_HEIGHT_CM, DataConstraints.MAX_WIDTH_CM).toDouble()
        filterExhibitsWeightFrom.value = 1.0
        filterExhibitsWeightTo.value = DataConstraints.MAX_WEIGHT_G.toDouble()
    }

    private fun updateFilters() {
        filterExhibits(isFilterExhibitsNames.value, filterExhibitsNames.value,
                isFilterExhibitsArtists.value, filterExhibitsArtists.value,
                isFilterExhibitsType.value, filterExhibitsType.value,
                isFilterExhibitsValue.value, filterExhibitsValueFrom.value.toInt(), filterExhibitsValueTo.value.toInt(),
                isFilterExhibitsSize.value, filterExhibitsSizeFrom.value.toInt(), filterExhibitsSizeTo.value.toInt(),
                isFilterExhibitsWeight.value, filterExhibitsWeightFrom.value.toInt(), filterExhibitsWeightTo.value.toInt())
    }


    private fun filterExhibits(filterNames: Boolean, namePrefix: String,
                               filterArtists: Boolean, artistsPrefix: String,
                               filterTypes: Boolean, typesPrefix: String,
                               filterValues: Boolean, valuesFrom: Int, valuesTo: Int,
                               filterSizes: Boolean, sizeFrom: Int, sizeTo: Int,
                               filterWeight: Boolean, weightFrom: Int, weightTo: Int) {
        tableExhibitsData.setAll(fullExhibitsData
                .filter { !filterArtists || it.artist?.toString()?.startsWithIgnoreCase(artistsPrefix) ?: false }
                .filter { !filterNames || it.title.startsWithIgnoreCase(namePrefix) }
                .filter { !filterTypes || it.type.startsWithIgnoreCase(typesPrefix) }
                .filter { !filterValues || (it.valuePln in valuesFrom..valuesTo) }
                .filter { !filterSizes || it.widthCm in sizeFrom..sizeTo || it.heightCm in sizeFrom..sizeTo }
                .filter { !filterWeight || it.weightG in weightFrom..weightTo }
        )
    }

    override val root = borderpane()

    private fun getTopRefreshed(artistData: ArtistData?) =
            borderpane {
                center = label("${messages["Art works of"]} $artistData") {
                    font = Font.font("Arial", FontWeight.BOLD, 20.0)
                }
                BorderPane.setMargin(center, Insets(10.0))
            }

    private fun getCenterRefreshed() =
            hbox {
                exhibitViewTable(tableExhibitsData) {
                    exhibitViewTable = this
                    hgrow = Priority.ALWAYS
                    vgrow = Priority.ALWAYS
                    onDoubleClick {
                        eventController.openHistoryEventLogWindow(exhibitViewTable.selectedItem, editMode)
                    }
                }
            }

    private fun Node.makeHideableForMod() {
        if (!editMode) {
            hide()
        }
    }

    private fun getRightRefreshed() =
            vbox {
                paddingAll = 10.0

                buttonMargin(messages["Refresh data"]) {
                    setOnAction {
                        exhibitController.refreshEventDataForLoaded(fullExhibitsData, loadedArtistData)
                        initFilters()
                    }
                }

                buttonMargin(messages["Edit selected"]) {
                    setOnAction {
                        exhibitController.openEditWindow(exhibitViewTable.selectedItem)
                    }
                    makeHideableForMod()
                }

                buttonMargin(messages["Create new"]) {
                    setOnAction {
                        exhibitController.openCreateWindow()
                    }
                    makeHideableForMod()
                }

                buttonMargin(messages["Remove selected"]) {
                    setOnAction {
                        exhibitController.deleteExhibitWithHistory(exhibitViewTable.selectedItem)
                        initFilters()
                    }
                    makeHideableForMod()
                }

                buttonMargin(messages["History"]) {
                    setOnAction {
                        eventController.openHistoryEventLogWindow(exhibitViewTable.selectedItem, editMode)
                    }
                }
            }

    private fun getLeftRefreshed() =
            vbox {
                paddingAll = 10.0
                smallTitleLabel(FX.messages["Filters"])
                vbox {
                    filterStrings(messages["Filter exhibits names"], isFilterExhibitsNames, filterExhibitsNames)
                    filterStrings(messages["Filter exhibits artists"], isFilterExhibitsArtists, filterExhibitsArtists)
                    filterStrings(messages["Filter exhibits type"], isFilterExhibitsType, filterExhibitsType)
                    filterIntRange("${messages["Filter value"]} [pln]", filterExhibitsValueFrom, filterExhibitsValueTo, isFilterExhibitsValue, 1, DataConstraints.MAX_VALUE_PLN)
                    filterIntRange("${messages["Filter size"]} [cm]", filterExhibitsSizeFrom, filterExhibitsSizeTo, isFilterExhibitsSize, 1, Integer.max(DataConstraints.MAX_WIDTH_CM, DataConstraints.MAX_HEIGHT_CM))
                    filterIntRange("${messages["Filter weight"]} [g]", filterExhibitsWeightFrom, filterExhibitsWeightTo, isFilterExhibitsWeight, 1, DataConstraints.MAX_WEIGHT_G)
                }
            }
}