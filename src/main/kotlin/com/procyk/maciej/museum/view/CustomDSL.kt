package com.procyk.maciej.museum.view

import com.procyk.maciej.museum.data.ArtistData
import com.procyk.maciej.museum.data.EventData
import com.procyk.maciej.museum.data.ExhibitData
import com.procyk.maciej.museum.view.icon.Icons
import com.procyk.maciej.museum.view.table.ArtistViewTable
import com.procyk.maciej.museum.view.table.EventViewTable
import com.procyk.maciej.museum.view.table.ExhibitViewTable
import javafx.beans.property.*
import javafx.collections.ObservableList
import javafx.event.EventTarget
import javafx.geometry.Insets
import javafx.geometry.Orientation
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.*
import javafx.scene.layout.*
import javafx.scene.paint.Paint
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.stage.Stage
import javafx.stage.Window
import javafx.util.StringConverter
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.EqOp
import org.jetbrains.exposed.sql.ExpressionWithColumnType
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.SqlExpressionBuilder.isNull
import org.jetbrains.exposed.sql.SqlExpressionBuilder.wrap
import tornadofx.*
import tornadofx.FX.Companion.messages
import tornadofx.controlsfx.rangeslider
import tornadofx.controlsfx.toggleswitch
import java.time.LocalDate


internal inline fun <T : Node> T.attachTo(
        parent: EventTarget,
        after: T.() -> Unit,
        before: (T) -> Unit
) = this.also(before).attachTo(parent, after)

fun TabPane.notClosableTab(text: String? = null, tag: Any? = null, op: Tab.() -> Unit = {}): Tab {
    val tab = Tab(text ?: tag?.toString())
    tab.tag = tag
    tab.isClosable = false
    tabs.add(tab)
    return tab.also(op)
}

fun EventTarget.buttonMargin(text: String = "", marginValue: Double = 3.0, prefWidth: Double = 150.0, graphic: Node? = null, op: Button.() -> Unit = {}) = Button(text).attachTo(this, op) {
    if (graphic != null) it.graphic = graphic

    it.hboxConstraints {
        margin = Insets(2 * marginValue, marginValue, 2 * marginValue, marginValue)
    }

    it.vboxConstraints {
        margin = Insets(2 * marginValue, marginValue, 2 * marginValue, marginValue)
    }

    it.prefWidth = prefWidth
    it.minWidth = prefWidth
}

fun EventTarget.positiveSlider(
        max: Number,
        value: Number? = null,
        orientation: Orientation? = null,
        includeZero: Boolean = false,
        op: Slider.() -> Unit = {}
) = slider(min = if (includeZero) 0 else 1, max = max, value = value, orientation = orientation, op = op)

fun EventTarget.positiveSpinner(property: Property<Number>, max: Int? = null, initialValue: Int? = null, amountToStepBy: Int? = null, editable: Boolean = false, enableScroll: Boolean = true, op: Spinner<Number>.() -> Unit = {}): Spinner<Number> {
    val spinner = Spinner<Number>(1, max ?: 100, initialValue ?: 0, amountToStepBy ?: 1)
    spinner.valueFactory.valueProperty().bindBidirectional(property)
    ViewModel.register(spinner.valueFactory.valueProperty(), property)
    spinner.isEditable = editable

    if (enableScroll) {
        spinner.setOnScroll { event ->
            if (event.deltaY > 0) spinner.increment()
            if (event.deltaY < 0) spinner.decrement()
        }
    }

    if (editable) {
        spinner.focusedProperty().addListener { _, _, newValue ->
            if (!newValue) {
                spinner.increment(0)
            }
        }
    }

    return spinner.attachTo(this, op)
}

infix fun <T:Comparable<T>> ExpressionWithColumnType<EntityID<T>?>.eqNullable(t: T?) : Op<Boolean> {
    if (t == null) {
        return isNull()
    }
    return EqOp(this, wrap(t))
}

fun EventTarget.artistViewTable(items: ObservableList<ArtistData>, op: ArtistViewTable.() -> Unit = {}) = ArtistViewTable(items).attachTo(this, op)

fun EventTarget.exhibitViewTable(items: ObservableList<ExhibitData>, hideArtist: Boolean = false, op: ExhibitViewTable.() -> Unit = {}) = ExhibitViewTable(items, hideArtist).attachTo(this, op)

fun EventTarget.eventViewTable(items: ObservableList<EventData>, hideExhibit: Boolean = false, op: EventViewTable.() -> Unit = {}) = EventViewTable(items, hideExhibit).attachTo(this, op)

fun TextField.maxLength(maxLength: Int = 0) {
    if (maxLength > 0) {
        textProperty().onChange { it?.let { text ->
            if (text.length > maxLength) {
                this.text = text.substring(0, maxLength)
            }
        } }
    }
}

fun EventTarget.saveCancelButtons(saveAction: () -> Unit = {}, cancelAction: () -> Unit = {}) = hbox {
    buttonMargin(messages["Save"], prefWidth = 100.0) {
        isDefaultButton = true
        setOnAction { saveAction() }
    }
    buttonMargin(messages["Cancel"], prefWidth = 100.0) {
        setOnAction { cancelAction() }
    }
}

fun EventTarget.createButton(saveAction: () -> Unit = {}) = hbox {
    buttonMargin(messages["Create"], prefWidth = 100.0) {
        setOnAction { saveAction() }
    }
}

fun EventTarget.proceedButton(saveAction: () -> Unit = {}) = hbox {
    buttonMargin(messages["Proceed"], prefWidth = 100.0) {
        setOnAction { saveAction() }
    }
}

fun EventTarget.cancelButton(canceAction: () -> Unit = {}) = hbox {
    buttonMargin(messages["Cancel"], prefWidth = 100.0) {
        setOnAction { canceAction() }
    }
}



inline fun customAlert(type: Alert.AlertType,
                       header: String? = null,
                       content: String? = null,
                       vararg buttons: ButtonType,
                       owner: Window? = null,
                       title: String? = null,
                       actionFn: Alert.(ButtonType) -> Unit = {}): Alert {

    val alert = Alert(type, content ?: "", *buttons)
    (alert.dialogPane?.scene?.window as? Stage)?.icons?.add(Icons.APP_ICON.image)
    title?.let { alert.title = it }
    alert.headerText = header
    owner?.also { alert.initOwner(it) }
    val buttonClicked = alert.showAndWait()
    if (buttonClicked.isPresent) {
        alert.actionFn(buttonClicked.get())
    }
    return alert
}

inline fun confirmDialog(content: String = messages["Do you really want to proceed with changes?"], confirmButton: ButtonType = ButtonType(messages["Yes"]), cancelButton: ButtonType = ButtonType(messages["Cancel"]), title: String = messages["Confirmation needed"], owner: Window? = null, actionFn: () -> Unit) {
    customAlert(type = Alert.AlertType.CONFIRMATION, content = content, buttons = *arrayOf(confirmButton, cancelButton), owner = owner, title = title) {
        if (it == confirmButton) actionFn()
    }
}

fun DatePicker.setDateRangeConstraints(disabledPredicate: (item: LocalDate) -> Boolean) {
    this.setDayCellFactory { object : DateCell() {
        override fun updateItem(item: LocalDate?, empty: Boolean) {
            super.updateItem(item, empty)
            isDisable = empty || item?.let { disabledPredicate(it) } ?: false
        }
    } }
}

fun EventTarget.dateFilterRange(updateStateAction: () -> Unit, filterDatesProperty: SimpleBooleanProperty, dateFromFilter: SimpleObjectProperty<LocalDate>, dateToFilter: SimpleObjectProperty<LocalDate>, spacing: Number? = 10.0, alignment: Pos? = null, op: VBox.() -> Unit = {}): VBox
        = vbox(spacing = spacing, alignment = alignment) {

    val filterDates = filterDatesProperty.also {
        it.onChange { updateStateAction() }
    }

    vbox(spacing = 7.0) {
        toggleswitch(messages["Filter dates"], filterDates) {
            padding = Insets(0.0, 0.0, 3.0, 0.0)
        }

        datepicker(dateFromFilter) {
            isEditable = false
            isShowWeekNumbers = false
            setDateRangeConstraints { item -> item.isAfter(dateToFilter.value) }
            dateToFilter.onChange { dateTo ->
                this.setDateRangeConstraints { item -> item.isAfter(dateTo) }
                updateStateAction()
            }
        }

        datepicker(dateToFilter) {
            isEditable = false
            isShowWeekNumbers = false
            setDateRangeConstraints { item -> item.isBefore(dateFromFilter.value) }
            dateFromFilter.onChange { dateTo ->
                this.setDateRangeConstraints { item -> item.isBefore(dateTo) }
                updateStateAction()
            }
        }
    }

    op()
}

fun EventTarget.filterStrings(title: String, isFilter: SimpleBooleanProperty, filterString: SimpleStringProperty) = vbox {
    paddingAll = 3.0

    toggleswitch(title, isFilter) {
        paddingAll = 5.0
    }
    textfield(filterString) {
        promptText = messages["starts with"]
        focusedProperty().addListener { _, _, focused ->
            if (focused) isFilter.value = true
        }
    }
}

fun EventTarget.filterIntRange(title: String, bottomProperty: SimpleDoubleProperty, upProperty: SimpleDoubleProperty, isFilterEnabled: SimpleBooleanProperty, minValue: Int, maxValue: Int) = vbox {

    toggleswitch(title, isFilterEnabled) {
        padding = Insets(10.0, 0.0, 5.0, 0.0)
    }

    rangeslider(bottomProperty, upProperty, minValue.toDouble(), maxValue.toDouble(), {})
    hbox {
        vbox {
            doubleToIntTextfield(bottomProperty, alignment = Pos.CENTER_LEFT)
            hgrow = Priority.ALWAYS
            alignment = Pos.CENTER_LEFT
        }
        vbox {
            doubleToIntTextfield(upProperty, alignment = Pos.CENTER_RIGHT)
            hgrow = Priority.ALWAYS
            alignment = Pos.CENTER_RIGHT
        }
    }
}

fun EventTarget.smallTitleLabel(text: String, fontSize: Double = 16.0, graphic: Node? = null, op: Label.() -> Unit = {}) = label(text = text, graphic = graphic) {
    padding = Insets(5.0, 0.0, 10.0, 0.0)
    font = Font.font("Arial", FontWeight.BOLD, fontSize)
    op()
}

object ToIntStringConverter : StringConverter<Number>() {

    override fun toString(data: Number?) = data?.toInt().toString()

    override fun fromString(string: String?): Number = string?.toDouble() ?: 0
}

fun EventTarget.doubleToIntTextfield(property: SimpleDoubleProperty, alignment: Pos = Pos.CENTER, isEditable: Boolean = false) = textfield(property, converter = ToIntStringConverter) {
    this.prefWidth = 50.0
    this.isEditable = isEditable
    this.alignment = alignment
    hboxConstraints { margin = Insets(5.0, 5.0, 5.0, 5.0) }
    vboxConstraints { margin = Insets(5.0, 5.0, 5.0, 5.0) }
}

fun Pane.borderSeparation() {
    hgrow = Priority.ALWAYS
    vgrow = Priority.ALWAYS
    hboxConstraints { margin = Insets(5.0, 5.0, 5.0, 5.0) }
    vboxConstraints { margin = Insets(5.0, 5.0, 5.0, 5.0) }
    border = Border(BorderStroke(Paint.valueOf("#000000"), BorderStrokeStyle.DOTTED, CornerRadii(10.0), BorderStroke.THIN))
}