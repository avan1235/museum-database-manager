package com.procyk.maciej.museum.view.table

import com.procyk.maciej.museum.app.Styles
import com.procyk.maciej.museum.data.ArtistData
import javafx.collections.ObservableList
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.control.TableView
import javafx.scene.paint.Color
import tornadofx.*
import tornadofx.FX.Companion.messages

class ArtistViewTable(items: ObservableList<ArtistData>) : TableView<ArtistData>(items) {

    init {
        readonlyColumn(messages["Name"], ArtistData::name).prefWidth(150.0)
        readonlyColumn(messages["Surname"], ArtistData::surname).prefWidth(150.0)
        readonlyColumn(messages["Is alive?"], ArtistData::isAlive) {
            addClass(Styles.centeredColumn)
            prefWidth(100.0)
        }.cellFormat {
            text = messages[if (it) "true" else "false"]
            style {
                if (it) {
                    backgroundColor += c("#3ea10d")
                    textFill = Color.WHITE
                } else {
                    backgroundColor += c("#db0707")
                    textFill = Color.WHITE
                }
            }
        }
        readonlyColumn(messages["Birth year"], ArtistData::birthYear).prefWidth(120.0)
        readonlyColumn(messages["Death year"], ArtistData::deathYear).prefWidth(120.0)

        vboxConstraints { margin = Insets(10.0) }
        hboxConstraints { margin = Insets(10.0) }

        placeholder = Label("${messages["Refresh data"]}...")
    }
}