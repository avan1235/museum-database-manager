package com.procyk.maciej.museum.view.icon

import javafx.scene.image.Image

enum class Icons(val image: Image) {
    APP_ICON(Image("icon.png"))
}