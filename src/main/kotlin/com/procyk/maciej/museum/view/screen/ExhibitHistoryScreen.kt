package com.procyk.maciej.museum.view.screen

import com.procyk.maciej.museum.controller.EventController
import com.procyk.maciej.museum.data.*
import com.procyk.maciej.museum.view.*
import com.procyk.maciej.museum.view.icon.Icons
import com.procyk.maciej.museum.view.table.EventViewTable
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Insets
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Priority
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.stage.Modality
import javafx.stage.StageStyle
import javafx.stage.Window
import tornadofx.*
import tornadofx.FX.Companion.messages
import tornadofx.controlsfx.toggleswitch
import java.time.LocalDate

class ExhibitHistoryScreen : View(messages["Exhibit History"]) {

    private val eventController: EventController by inject()

    private val fullEventData = mutableListOf<EventData>().observable()
    private val tableEventData = mutableListOf<EventData>().observable()
    private var loadedExhibitData: ExhibitData? = null
    private var editMode = false

    private val filterDates = SimpleBooleanProperty()
    private val filterFrom = SimpleObjectProperty(LocalDate.now())
    private val filterTo = SimpleObjectProperty(LocalDate.now())
    private val showExhibitions = SimpleBooleanProperty()
    private val showHires = SimpleBooleanProperty()
    private val showStorages = SimpleBooleanProperty()

    private lateinit var exhibitHistoryViewTable: EventViewTable

    init {
        primaryStage.icons += Icons.APP_ICON.image
        fullEventData.onChange { tableEventData.setAll(fullEventData) }

        listOf(showExhibitions, showStorages, showHires)
                .forEach { it.addListener { _, _, _ -> updateFilters() } }
    }

    fun reloadAndOpenWindow(
            exhibitData: ExhibitData,
            editMode: Boolean = false,
            stageStyle: StageStyle = StageStyle.DECORATED,
            modality: Modality = Modality.WINDOW_MODAL,
            escapeClosesWindow: Boolean = true,
            owner: Window? = currentWindow,
            block: Boolean = false,
            resizable: Boolean? = false) {

        initFilters()
        this.editMode = editMode
        loadedExhibitData = exhibitData
        fullEventData.setAll()
        eventController.refreshEventDataForLoaded(fullEventData, exhibitData)

        root.top = getTopRefreshed(exhibitData)
        root.center = getCenterRefreshed()
        root.right = getRightRefreshed()
        root.left = getLeftRefreshed()
        openWindow(stageStyle, modality, escapeClosesWindow, owner, block, resizable)
    }

    private fun initFilters() {
        showExhibitions.value = true
        showHires.value = true
        showStorages.value = true
        filterDates.value = false
        filterFrom.value = LocalDate.now()
        filterTo.value = LocalDate.now()
    }

    private fun updateFilters() {
        filterEvents(filterDates.value, filterFrom.value, filterTo.value)
    }

    private fun Iterable<EventData>.filterChecks(): List<EventData> {
        return this
                .filter { showExhibitions.value || !(it.describedEventData is ExhibitionEventData) }
                .filter { showHires.value || !(it.describedEventData is HireEventData) }
                .filter { showStorages.value || !(it.describedEventData is StoreEventData) }
    }

    private fun filterEvents(filterDates: Boolean, dateFrom: LocalDate, dateTo: LocalDate) {
        tableEventData.setAll(fullEventData
                .filter { !filterDates || (!(it.dateFrom.isAfter(dateTo) || it.dateTo.isBefore(dateFrom))) }
                .filterChecks())
    }

    override val root = borderpane()

    private fun getTopRefreshed(exhibitData: ExhibitData?) =
            borderpane {
                label(exhibitData?.toString() ?: messages["No exhibit found"]) {
                    center = this
                    font = Font.font("Arial", FontWeight.BOLD, 20.0)
                }
                BorderPane.setMargin(center, Insets(10.0))
            }

    private fun getCenterRefreshed() =
            hbox {
                eventViewTable(tableEventData, hideExhibit = true) {
                    exhibitHistoryViewTable = this
                    hgrow = Priority.ALWAYS
                    vgrow = Priority.ALWAYS
                }
            }

    private fun getRightRefreshed() =
            vbox {
                paddingAll = 10.0
                buttonMargin(messages["Refresh data"]) {
                    setOnAction {
                        eventController.refreshEventDataForLoaded(fullEventData, loadedExhibitData)
                        initFilters()
                    }
                }

                buttonMargin(messages["Edit selected"]) {
                    setOnAction {
                        eventController.openEditWindow(exhibitHistoryViewTable.selectedItem)
                        initFilters()
                    }
                    if (!editMode) hide()
                }

                buttonMargin(messages["Create new"]) {
                    setOnAction {
                        eventController.openCreateWindow(loadedExhibitData)
                        initFilters()
                    }
                    if (!editMode) hide()
                }

                buttonMargin(messages["Remove selected"]) {
                    setOnAction {
                        eventController.deleteHistoryEvent(exhibitHistoryViewTable.selectedItem)
                        eventController.refreshEventDataForLoaded(fullEventData, loadedExhibitData)
                        initFilters()
                    }
                    if (!editMode) hide()
                }
            }

    private fun getLeftRefreshed() =
            vbox {
                paddingAll = 10.0
                smallTitleLabel(FX.messages["Filters"])
                vbox(spacing = 10.0) {
                    dateFilterRange(this@ExhibitHistoryScreen::updateFilters, filterDates, filterFrom, filterTo) {
                        paddingAll = 10.0
                    }
                    toggleswitch(messages["Show exhibitions"], showExhibitions)
                    toggleswitch(messages["Show hires"], showHires)
                    toggleswitch(messages["Show storages"], showStorages)
                }
            }
}