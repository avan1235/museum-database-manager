package com.procyk.maciej.museum.view.screen

import com.procyk.maciej.museum.controller.ArtistController
import com.procyk.maciej.museum.data.ArtistData
import com.procyk.maciej.museum.data.DataConstraints
import com.procyk.maciej.museum.data.ExhibitData
import com.procyk.maciej.museum.view.*
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleIntegerProperty
import javafx.event.EventTarget
import javafx.geometry.Insets
import javafx.stage.Modality
import javafx.stage.StageStyle
import javafx.stage.Window
import tornadofx.*
import tornadofx.FX.Companion.messages
import java.time.LocalDate

class ArtistEditScreen : View(messages["Edit Artist"]) {

    private val artistController: ArtistController by inject()

    override val root = borderpane()

    private var editedArtistData: ArtistData = ArtistData.getEmpty()

    fun reloadAndOpenWindow(artistData: ArtistData,
                            stageStyle: StageStyle = StageStyle.DECORATED,
                            modality: Modality = Modality.WINDOW_MODAL,
                            escapeClosesWindow: Boolean = true,
                            owner: Window? = currentWindow,
                            block: Boolean = false,
                            resizable: Boolean? = false) {

        editedArtistData = artistData
        root.center = getCenterRefreshed()
        openWindow(stageStyle, modality, escapeClosesWindow, owner, block, resizable)
    }

    private fun getCenterRefreshed() = vbox {
        artistForm(editedArtistData, component =  this@ArtistEditScreen)
        saveCancelButtons(
                { artistController.saveEdited(editedArtistData) },
                { artistController.closeArtistEditScreen() })
    }
}

fun EventTarget.artistForm(artistData: ArtistData, component: UIComponent? = null, exhibitData: ExhibitData? = null, existingArtist: SimpleBooleanProperty? = null) = form {
        existingArtist?.addListener { _, _, exists ->
            if (!exists) exhibitData?.artist = artistData
        }
        var deathField: Field? = null
        val deathYearProperty = SimpleIntegerProperty(artistData.deathYear ?: artistData.birthYear)
        val isAlive = SimpleBooleanProperty(artistData.isAlive)

        isAlive.onChange { alive ->
            if (alive)   {
                deathField?.hide()
                artistData.deathYear = null
            }
            else {
                deathYearProperty.set(artistData.birthYear)
                deathField?.show()
            }
            component?.currentStage?.sizeToScene()
        }
        deathYearProperty.onChange {  deathYear ->
            if (artistData.birthYear > deathYear) {
                artistData.birthYearProperty.value = deathYear
            }
            if (!isAlive.value) {
                artistData.deathYear = deathYear
            }
        }
        artistData.birthYearProperty.onChange { birthYear ->
            if (deathYearProperty.value < birthYear) {
                deathYearProperty.value = birthYear
            }
        }

        fieldset(messages["Basic information"]) {
            field(messages["Name"]) {
                textfield(artistData.nameProperty) {
                    maxLength(DataConstraints.MAX_ARTIST_NAME_LENGTH)
                }
            }

            field(messages["Surname"]) {
                textfield(artistData.surnameProperty) {
                    maxLength(DataConstraints.MAX_ARTIST_SURNAME_LENGTH)
                }
            }
        }
        fieldset(messages["Memoir"]) {
            field(messages["Birth year"]) {
                positiveSpinner(artistData.birthYearProperty, max = LocalDate.now().year)
                positiveSlider(LocalDate.now().year).bind(artistData.birthYearProperty)
            }

            checkbox(messages["Is alive?"], isAlive) {
                hboxConstraints { margin = Insets(3.0) }
                vboxConstraints { margin = Insets(3.0) }
            }

            deathField = field(messages["Death year"]) {
                positiveSpinner(deathYearProperty, max = LocalDate.now().year)
                positiveSlider(LocalDate.now().year).bind(deathYearProperty)
            }
            deathField?.let {
                if (isAlive.value) it.hide() else it.show()
            }
        }
    }