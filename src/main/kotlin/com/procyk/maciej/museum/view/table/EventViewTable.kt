package com.procyk.maciej.museum.view.table

import com.procyk.maciej.museum.data.EventData
import javafx.collections.ObservableList
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.control.TableView
import tornadofx.*
import tornadofx.FX.Companion.messages

class EventViewTable(items: ObservableList<EventData>, hideExhibit: Boolean = false) : TableView<EventData>(items) {

    init {
        readonlyColumn(messages["Exhibit"], EventData::exhibit) {
            isVisible = !hideExhibit
            prefWidth(if (hideExhibit) 0.0 else 200.0)
        }
        readonlyColumn(messages["To"], EventData::dateTo) {
            prefWidth = 100.0
        }
        readonlyColumn(messages["From"], EventData::dateFrom) {
            prefWidth = 100.0
        }
        readonlyColumn(messages["Description"], EventData::describedEventData) {
            prefWidth = 350.0
        }.cellFormat {
            text = it.description
        }

        vboxConstraints { margin = Insets(10.0) }
        hboxConstraints { margin = Insets(10.0) }

        placeholder = Label("${messages["Refresh data"]}...")
    }
}