package com.procyk.maciej.museum.view.screen

import com.procyk.maciej.museum.controller.EventController
import com.procyk.maciej.museum.controller.HistoryEventType
import com.procyk.maciej.museum.data.*
import com.procyk.maciej.museum.view.buttonMargin
import com.procyk.maciej.museum.view.maxLength
import com.procyk.maciej.museum.view.saveCancelButtons
import com.procyk.maciej.museum.view.setDateRangeConstraints
import javafx.collections.ObservableList
import javafx.event.EventTarget
import javafx.geometry.Insets
import javafx.scene.control.DateCell
import javafx.scene.control.DatePicker
import javafx.scene.layout.BorderPane
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.stage.Modality
import javafx.stage.StageStyle
import javafx.stage.Window
import tornadofx.FX.Companion.messages
import tornadofx.*
import tornadofx.controlsfx.segmentedbutton
import java.time.LocalDate

class HistoryEventEditScreen : View(messages["Edit Event"]) {

    private val eventController: EventController by inject()

    private var editedEventData: EventData = EventData.getEmpty()
    private var originalDescribedEventData: DescribedEventData = ExhibitionEventData.getEmpty()

    private var eventTypeSelected: HistoryEventType = HistoryEventType.ExhibitionEvent
    private var storeEventData: StoreEventData = StoreEventData.getEmpty()
    private var hireEventData: HireEventData = HireEventData.getEmpty()
    private var exhibitionEventData: ExhibitionEventData = ExhibitionEventData.getEmpty()

    private val possibleDepots = mutableListOf<DepotData>().observable()
    private val possibleInstitutions = mutableListOf<InstitutionData>().observable()
    private val possibleRooms = mutableListOf<RoomData>().observable()


    override val root = borderpane()

    override fun onBeforeShow() {
        super.onBeforeShow()
        currentStage?.sizeToScene()
    }

    fun reloadAndOpenWindow(
            eventData: EventData,
            stageStyle: StageStyle = StageStyle.DECORATED,
            modality: Modality = Modality.WINDOW_MODAL,
            escapeClosesWindow: Boolean = true,
            owner: Window? = currentWindow,
            block: Boolean = false,
            resizable: Boolean? = false) {

        editedEventData = eventData
        originalDescribedEventData = eventData.describedEventData.copy()
        when(val currentEventData = eventData.describedEventData) {
            is StoreEventData -> {
                storeEventData = currentEventData.copyDetailed()
                eventTypeSelected = HistoryEventType.StoreEvent
            }
            is HireEventData -> {
                hireEventData = currentEventData.copyDetailed()
                eventTypeSelected = HistoryEventType.HireEvent
            }
            is ExhibitionEventData -> {
                exhibitionEventData = currentEventData.copyDetailed()
                eventTypeSelected = HistoryEventType.ExhibitionEvent
            }
        }

        root.center = getCenterRefreshed()
        runAsync {
            possibleDepots.setAll(eventController.getPossibleDepots())
            possibleInstitutions.setAll(eventController.getPossibleInstitutions())
            possibleRooms.setAll(eventController.getPossibleRooms())
        }
        openWindow(stageStyle, modality, escapeClosesWindow, owner, block, resizable)
    }

    private fun getCenterRefreshed() = form {
        label("${messages["History of"]} \"${editedEventData.exhibit.title}\"") {
            font = Font.font("Arial", FontWeight.BOLD, 18.0)
            padding = Insets(5.0, 10.0, 15.0, 0.0)
        }

        fieldset(messages["Time range"]) {
            field(messages["From"]) {
                datepicker(editedEventData.dateFromProperty) {
                    isEditable = false
                    isShowWeekNumbers = false
                    setDateRangeConstraints { item -> item.isAfter(editedEventData.dateTo) }
                    editedEventData.dateToProperty.onChange { dateTo ->
                        this.setDateRangeConstraints { item -> item.isAfter(dateTo) }
                    }
                }
            }

            field(messages["To"]) {
                datepicker(editedEventData.dateToProperty) {
                    isEditable = false
                    isShowWeekNumbers = false
                    setDateRangeConstraints { item -> item.isBefore(editedEventData.dateFrom) }
                    editedEventData.dateFromProperty.onChange { dateFrom ->
                        this.setDateRangeConstraints { item -> item.isBefore(dateFrom) }
                    }
                }
            }
        }

        fieldset(messages["Event details"]) {
            field(messages["Type"]) {
                hbox {
                    paddingAll = 5.0
                    togglegroup {
                        selectedValueProperty<String>().onChange { selectedEvent ->
                            when(selectedEvent) {
                                messages["Exhibition"] -> {
                                    eventTypeSelected = HistoryEventType.ExhibitionEvent
                                    root.bottom = exhibitionEventForm(exhibitionEventData, possibleRooms)
                                }
                                messages["Hire"] -> {
                                    eventTypeSelected = HistoryEventType.HireEvent
                                    root.bottom = hireEventForm(hireEventData, possibleInstitutions)
                                }
                                messages["Storage"] -> {
                                    eventTypeSelected = HistoryEventType.StoreEvent
                                    root.bottom = storeEventForm(storeEventData, possibleDepots)
                                }
                            }
                            root.bottom += saveCancelButtons(
                                    { eventController.saveEdited(editedEventData, storeEventData, hireEventData, exhibitionEventData, eventTypeSelected) },
                                    { eventController.closeHistoryEventEditScreen() })
                            BorderPane.setMargin(root.bottom, Insets(-25.0, 0.0, 0.0, 0.0))
                            this@HistoryEventEditScreen.currentStage?.sizeToScene()
                        }

                        radiobutton(messages["Exhibition"], this) {
                            paddingAll = 5.0
                            if (originalDescribedEventData is ExhibitionEventData) isSelected = true
                        }

                        radiobutton(messages["Hire"], this) {
                            paddingAll = 5.0
                            if (originalDescribedEventData is HireEventData) isSelected = true
                        }

                        radiobutton(messages["Storage"], this) {
                            paddingAll = 5.0
                            if (originalDescribedEventData is StoreEventData) isSelected = true
                        }
                    }
                }
            }
        }
    }
}

private fun EventTarget.exhibitionEventForm(exhibitionEventData: ExhibitionEventData, rooms: ObservableList<RoomData>) = form {
    fieldset {
        field(messages["Room"]) {
            combobox<RoomData>(exhibitionEventData.roomDataProperty, rooms) {
                if (exhibitionEventData == ExhibitionEventData.EMPTY)  selectionModel.selectFirst()
                rooms.onChange { this.items = rooms }
            }
        }
    }
}

private fun EventTarget.hireEventForm(hireEventData: HireEventData, institutions: ObservableList<InstitutionData>) = form {
    fieldset {
        field(messages["Institution"]) {
            combobox<InstitutionData>(hireEventData.institutionDataProperty, institutions) {
                if (hireEventData == HireEventData.EMPTY) selectionModel.selectFirst()
                institutions.onChange { this.items = institutions }
            }
        }
    }
}

private fun EventTarget.storeEventForm(storeEventData: StoreEventData, depots: ObservableList<DepotData>) = form {
    fieldset {
        field(messages["Depot"]) {
            combobox<DepotData>(storeEventData.depotDataProperty, depots) {
                if (storeEventData == StoreEventData.EMPTY) selectionModel.selectFirst()
                depots.onChange { this.items = depots }
            }
        }
        field(messages["Store case"]) {
            textfield(storeEventData.storeCaseProperty) {
                maxLength(DataConstraints.MAX_STORE_CASE_LENGTH)
            }
        }
    }
}