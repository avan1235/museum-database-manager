package com.procyk.maciej.museum.view.screen

import com.procyk.maciej.museum.app.Styles.Companion.loginScreen
import com.procyk.maciej.museum.controller.LanguageController
import com.procyk.maciej.museum.controller.LoginController
import com.procyk.maciej.museum.view.buttonMargin
import com.procyk.maciej.museum.view.icon.Icons
import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.geometry.Pos
import javafx.stage.Stage
import javafx.util.Duration
import tornadofx.*

class LoginScreen : View("Login") {
    private val loginController: LoginController by inject()
    private val languageController: LanguageController by inject()

    private val model = object : ViewModel() {
        val username = bind { SimpleStringProperty("") }
        val password = bind { SimpleStringProperty("") }
    }

    init {
        languageController.loadLanguageFromConfig()
        primaryStage.icons += Icons.APP_ICON.image
    }

    override val root = form {
        addClass(loginScreen)
        primaryStage.isResizable = false

        fieldset {
            field(messages["Username"]) {
                textfield(model.username) {
                    required(message = messages["Required field"])
                    whenDocked { requestFocus() }
                }
            }
            field(messages["Password"]) {
                passwordfield(model.password).required(message = messages["Required field"])
            }
        }

        hbox {
            alignment = Pos.CENTER
            buttonMargin(messages["Login"], prefWidth = 100.0) {
                isDefaultButton = true
                action {
                    loginController.tryLogin(
                            model.username.value,
                            model.password.value
                    )
                }
            }
            buttonMargin(messages["Exit"], prefWidth = 100.0) {
                action { Platform.exit() }
            }
        }
    }

    override fun onDock() {
        model.validate(decorateErrors = false)
    }

    fun shake() {
        val move = 5
        val keyframeDuration = Duration.seconds(0.03)

        val timelineX = buildSingleAxisShaker(keyframeDuration, { it.x += move; it }, { it.x -= move; it })
        val timelineY = buildSingleAxisShaker(keyframeDuration, { it.y += move; it }, { it.y -= move; it })

        timelineX.play()
        timelineY.play()
    }

    private fun buildSingleAxisShaker(keyframeDuration: Duration, stgModPlus: (stage: Stage) -> Stage, stgModMinus: (stage: Stage) -> Stage,  cycleCount: Int = 10) : Timeline {
        var i = 0
        var stage = FX.primaryStage
        val timeline = Timeline(KeyFrame(keyframeDuration, EventHandler {
            if (i == 0) {
                stage = stgModPlus(stage)
                i = 1
            } else {
                stage = stgModMinus(stage)
                i = 0
            }
        }))

        timeline.cycleCount = cycleCount
        timeline.isAutoReverse = false

        return timeline
    }

    fun clear() {
        model.username.value = ""
        model.password.value = ""
    }
}