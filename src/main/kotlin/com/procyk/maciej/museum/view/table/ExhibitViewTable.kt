package com.procyk.maciej.museum.view.table

import com.procyk.maciej.museum.app.Styles.Companion.centeredColumn
import com.procyk.maciej.museum.data.ExhibitData
import javafx.collections.ObservableList
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.control.TableView
import tornadofx.*
import tornadofx.FX.Companion.messages

class ExhibitViewTable(items: ObservableList<ExhibitData>, hideArtist: Boolean = false) : TableView<ExhibitData>(items) {

    init {
        readonlyColumn(messages["Title"], ExhibitData::title) {
            prefWidth(150.0)
        }
        readonlyColumn(messages["Type"], ExhibitData::type) {
            prefWidth(80.0)
        }
        readonlyColumn("${messages["Width"]} [cm]", ExhibitData::widthCm) {
            addClass(centeredColumn)
            prefWidth(100.0)
        }
        readonlyColumn("${messages["Height"]} [cm]", ExhibitData::heightCm) {
            addClass(centeredColumn)
            prefWidth(100.0)
        }
        readonlyColumn("${messages["Weight"]} [g]", ExhibitData::weightG) {
            addClass(centeredColumn)
            prefWidth(100.0)
        }
        readonlyColumn("${messages["Value"]} [pln]", ExhibitData::valuePln) {
            addClass(centeredColumn)
            prefWidth(100.0)
        }
        readonlyColumn(messages["Artist"], ExhibitData::artist) {
            prefWidth(if (hideArtist) 0.0 else 150.0)
            if (hideArtist) hide()
        }

        vboxConstraints { margin = Insets(10.0) }
        hboxConstraints { margin = Insets(10.0) }

        placeholder = Label("${messages["Refresh data"]}...")
    }
}

